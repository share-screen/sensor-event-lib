package swaiotos.sensor.input;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;

/**
 * @Author: yuzhan
 */
public class SimpleTouchData implements Serializable {
    public long downTime;
    public long eventTime;
    public int action;
    public float x, y;
    public int metaState;

    public int getMetaState() {
        return metaState;
    }

    public void setMetaState(int metaState) {
        this.metaState = metaState;
    }

    public long getDownTime() {
        return downTime;
    }

    public void setDownTime(long downTime) {
        this.downTime = downTime;
    }

    public long getEventTime() {
        return eventTime;
    }

    public void setEventTime(long eventTime) {
        this.eventTime = eventTime;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public String toJson() {
        return JSON.toJSONString(this);
    }
}
