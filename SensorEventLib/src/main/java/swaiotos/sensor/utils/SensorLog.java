package swaiotos.sensor.utils;

import android.text.TextUtils;
import android.util.Log;

/**
 * @Author: yuzhan
 */
public class SensorLog {

    public static String logID = null;

    public static void setLogID(String id) {
        if(!TextUtils.isEmpty(id)) {
            logID = "[" + id + "] ";
        }
    }

    public static void i(String TAG, String msg) {
        if(logID == null) {
            Log.i(TAG, msg);
        } else {
            Log.i(TAG, new StringBuilder(logID).append(msg).toString());
        }
    }

    public static void i(String id, String TAG, String msg) {
        if(id == null) {
            Log.i(TAG, msg);
        } else {
            Log.i(TAG, new StringBuilder("[").append(id).append("]").append(msg).toString());
        }
    }

    public static void d(String TAG, String msg) {
        if(logID == null) {
            Log.i(TAG, msg);
        } else {
            Log.d(TAG, new StringBuilder(logID).append(msg).toString());
        }
    }

    public static void d(String id, String TAG, String msg) {
        if(id == null) {
            Log.d(TAG, msg);
        } else {
            Log.d(TAG, new StringBuilder("[").append(id).append("]").append(msg).toString());
        }
    }

    public static void w(String TAG, String msg) {
        if(logID == null) {
            Log.i(TAG, msg);
        } else {
            Log.w(TAG, new StringBuilder(logID).append(msg).toString());
        }
    }

    public static void w(String id, String TAG, String msg) {
        if(id == null) {
            Log.w(TAG, msg);
        } else {
            Log.w(TAG, new StringBuilder("[").append(id).append("]").append(msg).toString());
        }
    }

    public static void e(String TAG, String msg) {
        if(logID == null) {
            Log.i(TAG, msg);
        } else {
            Log.e(TAG, new StringBuilder(logID).append(msg).toString());
        }
    }

    public static void e(String id, String TAG, String msg) {
        if(id == null) {
            Log.e(TAG, msg);
        } else {
            Log.e(TAG, new StringBuilder("[").append(id).append("]").append(msg).toString());
        }
    }
}
