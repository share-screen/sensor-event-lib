package swaiotos.sensor.channel;

/**
 * @Author: yuzhan
 */
public class P2PMsgSender {
    public IMsgSender sender;
    public String targetId;
    public boolean isP2P; //通道im是否是p2p的
    public boolean enableP2P; //业务是否启用p2p的

    public void sendMsg(String msg) throws Exception{
        sender.sendMsg(msg, targetId);
    }

    public void sendIntensiveMsg(String msg) throws Exception{
        sender.sendIntensiveMsg(msg, targetId);
    }
}
