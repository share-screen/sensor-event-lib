package swaiotos.sensor.channel;

/**
 * @Author: yuzhan
 */
public interface IMsgSender {
    boolean isChannelReady();
    void sendMsg(String content, String targetId) throws Exception;
    void sendMsg(String content, String targetId, boolean withProtoVersion) throws Exception;

    /**
     * 发送密集型数据，通道特殊处理
     * @param content
     * @param targetId
     * @throws Exception
     */
    void sendIntensiveMsg(String content, String targetId) throws Exception;
    void sendMsgSticky(String content, String targetId) throws Exception;
    void sendMsgSticky(String content, String targetId, boolean withProtoVersion) throws Exception;
    void setProtoVersion(int protoVersion);
    boolean supportImP2P();
}
