package swaiotos.sensor.channel;

import android.content.Context;
import android.os.DeadObjectException;
import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;

import swaiotos.channel.iot.IOTChannel;
import swaiotos.channel.iot.ss.SSChannel;
import swaiotos.channel.iot.ss.channel.im.IMMessage;
import swaiotos.channel.iot.ss.channel.im.IMMessageCallback;
import swaiotos.channel.iot.ss.session.Session;
import swaiotos.sensor.data.AccountInfo;
import swaiotos.sensor.utils.SensorLog;

/**
 * @Author: yuzhan
 */
public class ChannelMsgSender implements IMsgSender{
    protected Context context;
    protected SSChannel ssChannel;
    protected String clientId;

    protected String stickyMsg;
    protected String stickyTargetId;
    private AccountInfo accountInfo;

    private int protoVersion = -1;
    private boolean showTips;

    public static String TAG = "SSCClient";

    public ChannelMsgSender(Context context, String clientId) {
        this.context = context;
        this.clientId = clientId;
        TAG = logTag();
        SensorLog.d(clientId, TAG, "ChannelMsgSender init channel clientId=" + clientId + ", pkg=" + iotPkgName());
        open();
    }

    protected String logTag() {
        return "SSCClient";
    }

    public void open() {
        SensorLog.d(clientId, TAG, "bind iot-channel " + iotPkgName());
        IOTChannel.mananger.open(context.getApplicationContext(), iotPkgName(), new IOTChannel.OpenCallback() {
            @Override
            public void onConntected(SSChannel channel) {
                SensorLog.d(clientId, TAG, "ChannelMsgSender init channel onConnected");
                ssChannel = channel;
                if(!TextUtils.isEmpty(stickyMsg)) {
                    try {
                        sendMsg(stickyMsg, stickyTargetId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(String s) {
                SensorLog.d(clientId, TAG, "ChannelMsgSender init channel onError : " + s);
                ssChannel = null;
            }
        });
    }

    @Override
    public void setProtoVersion(int protoVersion) {
        this.protoVersion = protoVersion;
    }

    @Override
    public boolean supportImP2P() {
        if(ssChannel != null) {
            try {
                return ssChannel.getSessionManager().getMySession().supportP2P();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public void setShowTips(boolean showTips) {
        this.showTips = showTips;
    }

    public void setTag(String tag) {
        TAG = tag;
    }

    @Override
    public boolean isChannelReady() {
        return ssChannel != null;
    }

    @Override
    public void sendMsg(String content, String targetId) throws Exception {
        sendMsg(content, targetId, true);
    }

    @Override
    public void sendMsg(String content, String targetId, boolean withProtoVersion) throws Exception {
        sendMsg(content, targetId, withProtoVersion, false);
    }

    protected void sendMsg(String content, String targetId, boolean withProtoVersion, boolean intensive) throws Exception {
        try {
            Session sourceSession = ssChannel.getSessionManager().getMySession();
            Session targetSession = null;
            try {
                targetSession = ssChannel.getSessionManager().getConnectedSession();
            } catch (Exception e) {
                e.printStackTrace();
            }

//            SensorLog.d(TAG, "sourceSession=" + sourceSession + ", targetSession=" + targetSession);

            IMMessage message = new IMMessage.Builder()
                    .setSource(sourceSession)
                    .setTarget(targetSession)
                    .setClientSource(clientId)
                    .setClientTarget(targetId)
                    .setContent(content)
                    .setType(IMMessage.TYPE.TEXT)
                    .build();
            //.setType(intensive ? IMMessage.TYPE.PROTO : IMMessage.TYPE.TEXT)//通道说改成PROTO可以提升性能，但是实际测试会出现一些乱七八糟的问题
            if(withProtoVersion && protoVersion >= 0)
                message.setReqProtoVersion(protoVersion);
            if(showTips) {
                message.putExtra("showtips", "true");
                if(accountInfo != null) {
                    JSONObject owner = new JSONObject();
                    owner.put("userID", accountInfo.open_id);
                    owner.put("token", accountInfo.accessToken);
                    owner.put("mobile", accountInfo.mobile);
                    owner.put("nickName", accountInfo.nickName);
                    owner.put("avatar", accountInfo.avatar);
                    message.putExtra("owner", owner.toJSONString());
                }
            }
            if(intensive) {
                //密集型数据，不上报日志
                message.putExtra(SSChannel.REPORT_MESSAGE_OFF, SSChannel.REPORT_MESSAGE_OFF_VALUE);
            }

            SensorLog.d(clientId, TAG, "sendMsg : " + message);
            ssChannel.getIMChannel().send(message, callback);
        } catch (DeadObjectException e) {
            e.printStackTrace();
            SensorLog.d(clientId, TAG, "with DeadObjectException, try to re open.");
            open();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送密集型数据，通道特殊处理
     *
     * @param content
     * @param targetId
     * @throws Exception
     */
    @Override
    public void sendIntensiveMsg(String content, String targetId) throws Exception {
        sendMsgSticky(content, targetId, false, true);
    }

    @Override
    public void sendMsgSticky(String content, String targetId) throws Exception {
        sendMsgSticky(content, targetId, true);
    }

    @Override
    public void sendMsgSticky(String content, String targetId, boolean withProtoVersion) throws Exception {
        sendMsgSticky(content, targetId, withProtoVersion, false);
    }

    protected void sendMsgSticky(String content, String targetId, boolean withProtoVersion, boolean intensiveMsg) throws Exception {
        if(isChannelReady()) {
            sendMsg(content, targetId, withProtoVersion, intensiveMsg);
        } else {
            stickyMsg = content;
            stickyTargetId = targetId;
        }
    }

    protected IMMessageCallback callback = new IMMessageCallback() {

        @Override
        public void onStart(IMMessage message) {

        }

        @Override
        public void onProgress(IMMessage message, int progress) {

        }

        @Override
        public void onEnd(IMMessage message, int code, String info) {
        }
    };

    protected String iotPkgName() {
        return context.getPackageName();
    }
}
