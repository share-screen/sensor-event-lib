package swaiotos.sensor.touch;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;


import swaiotos.sensor.client.IConnectClient;

/**
 * @Author: yuzhan
 */
public class BarInputTouchView extends FrameLayout {
    //用于发送event事件
    private IConnectClient client;

    //激光笔是否已经展示，在激光笔界面如果有一个手指按下，其余按下手指都不发送event事件给dongle，也不展示图标
    private boolean isLaserPointShow = false;
    private static final String TAG = "InputTouchView";

    public BarInputTouchView(Context context) {
        super(context);
        setBackgroundColor(Color.DKGRAY);
        setClickable(true);
        setOnTouchListener(onTouchListener);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public void setSize(int w, int h) {
        setLayoutParams(new LayoutParams(w, h));
    }

    public void setClient(IConnectClient client) {
        this.client = client;
    }

    private OnTouchListener onTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (client == null) return false;
            client.sendMotionEvent(event, v, true);
            return false;
        }
    };

    public void onStart() {

    }

    public void onStop() {

    }

}
