package swaiotos.sensor.touch;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

/**
 * @Author: yuzhan
 */
public class SSSensorHelper {

    public interface SSSensorCallback {
        void onSensorChanged(SensorEvent event, float[] sensorValues);
        /**
         * 方向变化（加速度传感器）：
         * x：手机侧面立起来，屏幕向左是10，屏幕向右是-10
         * y：手机水平放置，屏幕向上是10，屏幕向下是-10
         * z：手机竖立起来，向上是10，向下是-10
         */
        void onSensorOrientationChanged(float x, float y, float z);
    }

    private SensorManager sensorManager;
    private Sensor sensor;
    private int sensorType;
    private boolean sensorStarted = false;
    private float[] sensorValues;
    private boolean enableSensor = false;
    private float sensorThreshold = 0;
    private SSSensorCallback callback;

    private Sensor orientationSensor;

    private static final String TAG = "SSSensor";

    public SSSensorHelper(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        orientationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);//方向的不可用了，用加速度处理
    }

    public void setSensorType(int type) {
        Log.d(TAG, "setSensorType : " + type);
        this.sensorType = type;
        sensor = sensorManager.getDefaultSensor(type);
    }

    public void setSensorEnable(boolean enable) {
        Log.d(TAG, "setSensorEnable : " + enable);
        enableSensor = enable;
        if (!enable) {
            stopSensor();
        }
    }

    public void setSensorThreshold(float t) {
        sensorThreshold = t;
    }

    public void setCallback(SSSensorCallback callback) {
        this.callback = callback;
    }

    public void startSensor() {
        if (enableSensor && sensor != null) {
            try {
                sensorManager.registerListener(sensorEventListener, sensor, SensorManager.SENSOR_DELAY_FASTEST);
                sensorManager.registerListener(orientationListener, orientationSensor, SensorManager.SENSOR_DELAY_NORMAL);
                sensorStarted = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void stopSensor() {
        sensorValues = null;
        try {
            sensorManager.unregisterListener(sensorEventListener);
            sensorStarted = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean needSendSensorEvent = false;
    private SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
//            Log.d(TAG, "onSensorChanged...type=" + event.sensor.getType());
            if (event.sensor.getType() != sensorType) {
                return;
            }
            needSendSensorEvent = false;
            if (sensorValues == null) {
                sensorValues = new float[3];
                sensorValues[0] = event.values[0];
                sensorValues[1] = event.values[1];
                sensorValues[2] = event.values[2];
            } else {
                if(sensorThreshold <= 0) {//不需要过滤小的变动
                    needSendSensorEvent = true;
                } else {
                    if (Math.abs(event.values[0] - sensorValues[0]) > sensorThreshold) {
                        sensorValues[0] = event.values[0];
                        needSendSensorEvent = true;
                    } else if (Math.abs(event.values[1] - sensorValues[1]) > sensorThreshold) {
                        sensorValues[1] = event.values[1];
                        needSendSensorEvent = true;
                    } else if (Math.abs(event.values[2] - sensorValues[2]) > sensorThreshold) {
                        sensorValues[2] = event.values[2];
                        needSendSensorEvent = true;
                    }
                }
            }
//            Log.d(TAG, "onSensorChanged...needSendSensorEvent=" + needSendSensorEvent);
            Log.d(TAG, "onSensorChanged, x=" + event.values[0] + ", y=" + event.values[1] + ", z=" + event.values[2]);
            if(needSendSensorEvent && callback != null) {
                callback.onSensorChanged(event, sensorValues);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    private float orientationThreshold = 2;
    private float[] orientationValues = new float[3];
    private boolean needSensOrientationEvent = true;
    //手机方向
    private SensorEventListener orientationListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
                return;
            }
            if(!needSensOrientationEvent) {
                if (Math.abs(event.values[0] - orientationValues[0]) > orientationThreshold) {
                    needSensOrientationEvent = true;
                } else if (Math.abs(event.values[1] - orientationValues[1]) > orientationThreshold) {
                    needSensOrientationEvent = true;
                } else if (Math.abs(event.values[2] - orientationValues[2]) > orientationThreshold) {
                    needSensOrientationEvent = true;
                }
            }
            if(needSensOrientationEvent) {
                orientationValues[0] = event.values[0];
                orientationValues[1] = event.values[1];
                orientationValues[2] = event.values[2];
                if(callback != null) {
                    callback.onSensorOrientationChanged(orientationValues[0], orientationValues[1], orientationValues[2]);
                }
            }
            needSensOrientationEvent = false;
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };
}
