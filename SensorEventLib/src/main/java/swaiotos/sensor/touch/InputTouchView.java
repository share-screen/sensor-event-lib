package swaiotos.sensor.touch;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.SensorEvent;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import java.util.HashMap;
import java.util.Map;

import swaiotos.sensor.client.IConnectClient;

/**
 * @Author: yuzhan
 */
public class InputTouchView extends FrameLayout implements SSSensorHelper.SSSensorCallback {
    //用于发送event事件
    private IConnectClient client;
    //是否需要展示两个反馈 用于区分激光笔(false)和照片投屏(true)
    private boolean needTwoFinger;
    //激光笔是否已经展示，在激光笔界面如果有一个手指按下，其余按下手指都不发送event事件给dongle，也不展示图标
    private boolean isLaserPointShow = false;
    private static final String TAG = "SSCClient";

    private SSSensorHelper ssSensorHelper;

    private SparseArray<TouchPoint> touchDrawableBeanSparseArray;

    private boolean touchEnable = true;


    private Map<String, String> extra;
    private float scaleX;
    private float scaleY;

    public InputTouchView(Context context) {
        super(context);
        setBackgroundColor(Color.DKGRAY);
        setClickable(true);
        setOnTouchListener(onTouchListener);
        touchDrawableBeanSparseArray = new SparseArray<>();

        ssSensorHelper = new SSSensorHelper(context);
        ssSensorHelper.setCallback(this);
    }

    public void setAnimatorValue(float value, int id) {
        TouchPoint touchPoint = touchDrawableBeanSparseArray.get(id);
        if (touchPoint != null) {
            touchPoint.setValue(value);
            invalidate();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (touchDrawableBeanSparseArray.size() > 0) {
            for (int i = 0; i < touchDrawableBeanSparseArray.size(); i++) {
                TouchPoint touchPoint = touchDrawableBeanSparseArray.get(i);
                if (touchPoint != null) {//友盟空指针保护
                    touchPoint.draw(canvas);
                }
            }
        }

    }

    public void addExtra(String key, String value) {
        if (extra == null) {
            extra = new HashMap<>();
        }
        extra.put(key, value);
    }

    public void setSize(int w, int h) {
        setLayoutParams(new FrameLayout.LayoutParams(w, h));
    }

    public void setClient(IConnectClient client) {
        this.client = client;
    }

    public InputTouchView setSensorType(int type) {
        ssSensorHelper.setSensorType(type);
        return this;
    }

    public InputTouchView setSensorEnable(boolean enable) {
        ssSensorHelper.setSensorEnable(enable);
        return this;
    }

    public InputTouchView setSensorThreshold(float t) {
        ssSensorHelper.setSensorThreshold(t);
        return this;
    }

    private View.OnTouchListener pOnTouchListener = null;//支持业务再增加一个touch
    public void addOnTouchListener(View.OnTouchListener listener) {
        this.pOnTouchListener = listener;
    }

    public void removeOnTouchListener(View.OnTouchListener listener) {
        this.pOnTouchListener = null;
    }

    public void setTouchEnable(boolean b) {
        Log.d(TAG, "!!! setTouchEnable : " + b);
        touchEnable = b;
    }

    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (client == null) {
                if(pOnTouchListener != null) {
                    return pOnTouchListener.onTouch(v, event);
                }
                return false;
            }
            boolean parentIntercept = false;
            if(pOnTouchListener != null) {
                parentIntercept = pOnTouchListener.onTouch(v, event);
            }
            if(!parentIntercept && touchEnable) {//支持父控件拦截
                controlTouchView(event);
                if (needTwoFinger) {
                    client.sendMotionEvent(event, v, extra);
                } else if (isLaserPointShow && !needTwoFinger) {
                    client.sendMotionEvent(event, v, extra);
                } else {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        client.sendMotionEvent(event, v, extra);
                    }
                }
            }

            return parentIntercept;
        }
    };

    private void controlTouchView(MotionEvent event) {
        int index = event.getActionIndex();
        int pointerId = event.getPointerId(index);
        TouchPoint touchPoint = touchDrawableBeanSparseArray.get(pointerId);
        //激光笔只添加一个点
        if (touchPoint == null && ((needTwoFinger && touchDrawableBeanSparseArray.size() < 2)
                || (!needTwoFinger && touchDrawableBeanSparseArray.size() == 0))) {
            touchPoint = new TouchPoint(getContext(), this, pointerId);
            touchDrawableBeanSparseArray.put(pointerId, touchPoint);
        }
        if (touchPoint == null) return;
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                stopSensor();
                if (pointerId == 0) {
                    isLaserPointShow = true;
                }
                if (pointerId < event.getPointerCount()) {
                    touchPoint.updatePosition(event.getX(pointerId), event.getY(pointerId));
                    touchPoint.addDrawableAnim();
                }
                break;
            case MotionEvent.ACTION_MOVE:
                for (int i = 0; i < event.getPointerCount(); i++) {
                    pointerId = event.getPointerId(i);
                    touchPoint = touchDrawableBeanSparseArray.get(pointerId);
                    if (touchPoint != null) {
                        touchPoint.updatePosition(event.getX(i), event.getY(i));
                    }
                }
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                if (pointerId == 0) {
                    isLaserPointShow = false;
                }
                touchPoint.removeDrawableAnim();
                break;
        }
    }

    public void setNeedTwoFinger(boolean needTwoFinger) {
        this.needTwoFinger = needTwoFinger;
    }

    public void onStart() {
    }

    public void onStop() {
        stopSensor();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
    }

    public void startSensor() {
        ssSensorHelper.startSensor();
    }

    public void stopSensor() {
        ssSensorHelper.stopSensor();
    }

    public void setScale(float scaleX,float scaleY){
        client.setScaleXY(scaleX,scaleY);
    }

    @Override
    public void onSensorChanged(SensorEvent event, float[] sensorValues) {
        client.sendSensorEvent(event, InputTouchView.this);
    }

    /**
     * 方向变化（加速度传感器）：
     * x：手机侧面立起来，屏幕向左是10，屏幕向右是-10
     * y：手机水平放置，屏幕向上是10，屏幕向下是-10
     * z：手机竖立起来，向上是10，向下是-10
     *
     * @param x
     * @param y
     * @param z
     */
    @Override
    public void onSensorOrientationChanged(float x, float y, float z) {
        client.onSensorOrientationChanged(x, y, z);
    }
}
