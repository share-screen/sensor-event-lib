package swaiotos.sensor.client;

import android.hardware.SensorEvent;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;
import swaiotos.sensor.channel.P2PMsgSender;
import swaiotos.sensor.connect.IConnectCallback;
import swaiotos.sensor.data.ClientCmdInfo;
import swaiotos.sensor.mgr.InfoManager;
import swaiotos.sensor.tm.TM;
import swaiotos.sensor.utils.MotionEventUtil;
import swaiotos.sensor.utils.SensorEventUtil;
import swaiotos.sensor.utils.SensorLog;

/**
 * @Author: yuzhan
 */
public class SensorConnectClient implements IConnectClient {

    private OkHttpClient client;
    private Request request;
    private IConnectCallback callback;
    private WebSocket socket;
    private volatile boolean isConnect;
    private int reConnectCount = 0;
    private final int RE_CONNECT_LIMIT_COUNT = 5;
    private String clientSSId;
    private InfoManager infoManager;
    private ISmartApi smartApi;
    private Map<String, String> extra;
    private boolean simpleMotionEventData = false;//是用精简的数据结构
    private float scaleX ;
    private float scaleY ;
    private P2PMsgSender p2pSender;

    private Object lock = new Object();

    private final String TAG = "SSCClient";

    public SensorConnectClient(InfoManager infoManager) {
        this.clientSSId = infoManager.getBusinessInfo().clientSSId;
        client = new OkHttpClient.Builder()
                .writeTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .pingInterval(10, TimeUnit.SECONDS)
                .build();

        this.infoManager = infoManager;
        scaleX = 1f;
        scaleY = 1f;
    }

    @Override
    public void setSmartApi(ISmartApi smartApi) {
        this.smartApi = smartApi;
    }

    public void setSimpleMotionEventData(boolean b) {
        simpleMotionEventData = b;
    }

    @Override
    public void setP2PSender(P2PMsgSender sender) {
        p2pSender = sender;
    }

    @Override
    public boolean isConnected() {
        SensorLog.d(clientSSId, TAG, "isConnected()=======" + (socket != null && isConnect));
        return socket != null && isConnect;
    }

    @Override
    public void connect(final String url, final IConnectCallback callback) {
        SensorLog.d(clientSSId, TAG, "client call connect url=" + url);
        TM.Companion.ioSingle(new Runnable() {
            @Override
            public void run() {
                if (isConnected()) {
                    SensorLog.d(clientSSId, TAG, "already connected.");
                    return;
                }
                SensorLog.d(clientSSId, TAG, "real start connect : " + url);
                reConnectCount = 0;
                SensorConnectClient.this.callback = callback;

                request = new Request.Builder().url(url).addHeader("clientId", clientSSId).build();
                connect();
            }
        });
    }


    @Override
    public void disconnect() {
        SensorLog.d(clientSSId, TAG, "client call disconnect, client=" + client);
        TM.Companion.ioSingle(new Runnable() {
            @Override
            public void run() {
                if (socket != null) {
                    synchronized (lock) {
                        if(socket != null) {
                            try {
                                socket.close(1000, "client-stop");
                            } catch (Exception e) {
                                SensorLog.d(clientSSId, TAG, "client call disconnect, error=" + e.toString());
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void send(final String text) {
        TM.Companion.ioSingle(new Runnable() {
            @Override
            public void run() {
                if (isConnected()) {
                    SensorLog.d(clientSSId, TAG, "ws send : " + text);
                    synchronized (lock) {
                        if(socket != null) {
                            socket.send(text);
                        }
                    }
                } else if(p2pSender != null) {
                    SensorLog.d(clientSSId, TAG, "ws not connect, check p2p=" + p2pSender.isP2P + ", supportP2P=" + p2pSender.enableP2P);
                    if(p2pSender.isP2P) {
                        try {
                            p2pSender.sendMsg(text);
                        } catch (Exception e) {
                            Log.w(TAG, "p2pSender send fail : " + e.toString());
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    //发送密集型数据
    public void sendIntensive(final String text) {
        TM.Companion.ioSingle(new Runnable() {
            @Override
            public void run() {
                if (isConnected()) {
                    SensorLog.d(clientSSId, TAG, "ws send : " + text);
                    synchronized (lock) {
                        if(socket != null) {
                            socket.send(text);
                        }
                    }
                } else if(p2pSender != null) {
                    SensorLog.d(clientSSId, TAG, "ws not connect, check p2p=" + p2pSender.isP2P + ", supportP2P=" + p2pSender.enableP2P);
                    if(p2pSender.isP2P) {
                        try {
                            p2pSender.sendIntensiveMsg(text);
                        } catch (Exception e) {
                            Log.w(TAG, "p2pSender sendIntensiveMsg fail : " + e.toString());
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void sendMotionEvent(MotionEvent event, View v) {
        sendMotionEvent(event, v, false);
    }

    @Override
    public void sendMotionEvent(MotionEvent event, View v, boolean isBar) {
//        SensorLog.d(TAG, "sendMotionEvent()=======getAction:" + event.getAction() + "---getActionMasked:" + event.getActionMasked() + ", isBar=" + isBar);
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                TM.Companion.ioSingle(new Runnable() {
                    @Override
                    public void run() {
                        if (smartApi != null && !smartApi.isSameWifi()) {
                            smartApi.startConnectSameWifi();
                        }
                    }
                });
                innerSendMotionEvent(event, v, isBar);
                break;
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_POINTER_UP:
                innerSendMotionEvent(event, v, isBar);
                break;
        }
    }

    @Override
    public void sendMotionEvent(MotionEvent event, View v, Map<String, String> extra) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                TM.Companion.ioSingle(new Runnable() {
                    @Override
                    public void run() {
                        if (smartApi != null && !smartApi.isSameWifi()) {
                            smartApi.startConnectSameWifi();
                        }
                    }
                });
                innerSendMotionEvent(event, v, extra);
                break;
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_POINTER_UP:
                innerSendMotionEvent(event, v, extra);
                break;
        }
    }

    private void innerSendMotionEvent(MotionEvent event, View v, boolean isBar) {
        ClientCmdInfo info = ClientCmdInfo.build(infoManager, ClientCmdInfo.CMD_CLIENT_MOTION_EVENT, v);
        info.content = MotionEventUtil.formatTouchEvent(event,1,1);
        info.addExtra("isBar", String.valueOf(isBar));
        info.setMsgP2p(p2pSender.isP2P);
        sendIntensive(JSON.toJSONString(info));
    }

    private void innerSendMotionEvent(MotionEvent event, View v, Map<String, String> extra) {
        ClientCmdInfo info = ClientCmdInfo.build(infoManager, ClientCmdInfo.CMD_CLIENT_MOTION_EVENT, v);
        if(simpleMotionEventData) {
            info.content = MotionEventUtil.simpleFormat(event);
        } else {
            info.content = MotionEventUtil.formatTouchEvent(event,scaleX,scaleY);
        }
        info.setMsgP2p(p2pSender.isP2P);
        info.extra = extra;
        sendIntensive(JSON.toJSONString(info));
    }

    @Override
    public void sendSensorEvent(SensorEvent event, View v) {
        ClientCmdInfo info = ClientCmdInfo.build(infoManager, ClientCmdInfo.CMD_CLIENT_SENSOR_EVENT, v);
        info.content = SensorEventUtil.formatSensorEvent(event);
        info.setMsgP2p(p2pSender.isP2P);
        sendIntensive(JSON.toJSONString(info));
    }

    @Override
    public void onSensorOrientationChanged(float x, float y, float z) {
        ClientCmdInfo info = ClientCmdInfo.build(infoManager, ClientCmdInfo.CMD_CLIENT_SENSOR_ORIENTATION_CHANGED);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("x", x);
        jsonObject.put("y", y);
        jsonObject.put("z", z);
        info.content = jsonObject.toJSONString();
        info.setMsgP2p(p2pSender.isP2P);
        sendIntensive(JSON.toJSONString(info));
    }

    @Override
    public void setScaleXY(float scaleX, float scaleY) {
        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }

    private void connect() {
        TM.Companion.ioSingle(new Runnable() {
            @Override
            public void run() {
                if (isConnected()) {
                    SensorLog.d(clientSSId, TAG, "already connected2.");
                    return;
                }
                WebSocket socket = client.newWebSocket(request, listener);
                SensorLog.d(clientSSId, TAG, "client newWebSocket : " + socket);
            }
        });
    }

    private boolean reconnect() {
        if (reConnectCount > RE_CONNECT_LIMIT_COUNT) {
            Log.i(TAG, "reconnect over limit : " + RE_CONNECT_LIMIT_COUNT + ", please check url or network");
            return false;
        } else {
            TM.Companion.removeIO(connectRunnable);
            TM.Companion.io(connectRunnable, 1000);
            return true;
        }
    }

    private Runnable connectRunnable = new Runnable() {
        @Override
        public void run() {
            connect();
            reConnectCount++;
        }
    };

    private void onSocketConnect() {
        callback.onSuccess();
    }

    private void onSocketDisconnect() {
        callback.onClose();
    }

    private WebSocketListener listener = new WebSocketListener() {
        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);
            SensorLog.d(clientSSId, TAG, "connect, onOpen : " + response.toString() + ", webSocket=" + webSocket);
            synchronized (lock) {
                socket = webSocket;
            }
            isConnect = response.code() == 101;
            if (!isConnect) {
                reconnect();
            } else {
                onSocketConnect();
            }
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            SensorLog.d(clientSSId, TAG, "connect, onMessage : " + text);
            super.onMessage(webSocket, text);
            callback.onMessage(text);
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            super.onMessage(webSocket, bytes);
            onMessage(webSocket, bytes.base64());
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            SensorLog.d(clientSSId, TAG, "connect, onClosing : code=" + code + ", reason=" + reason);
            super.onClosing(webSocket, code, reason);
            synchronized (lock) {
                socket = null;
            }
            isConnect = false;
            onSocketDisconnect();
        }

        @Override
        public void onClosed(WebSocket webSocket, int code, String reason) {
            SensorLog.d(clientSSId, TAG, "connect, onClosed : code=" + code + ", reason=" + reason);
            super.onClosed(webSocket, code, reason);
            synchronized (lock) {
                socket = null;
            }
            isConnect = false;
            onSocketDisconnect();
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            SensorLog.d(clientSSId, TAG, "connect onFailure：" + response);
            super.onFailure(webSocket, t, response);
            isConnect = false;

            if (t != null) {
                t.printStackTrace();
            }
            callback.onFailOnce(t.toString());
            if (!reconnect()) {
                callback.onFail(t.toString());
            }
        }
    };
}
