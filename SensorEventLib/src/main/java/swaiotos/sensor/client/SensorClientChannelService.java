package swaiotos.sensor.client;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import swaiotos.channel.iot.ss.SSChannel;
import swaiotos.channel.iot.ss.SSChannelClient;
import swaiotos.channel.iot.ss.channel.im.IMMessage;
import swaiotos.sensor.data.ChannelEvent;
import swaiotos.sensor.utils.SensorLog;

/**
 * @Author: yuzhan
 */
public class SensorClientChannelService extends SSChannelClient.SSChannelClientService {

    private static final String TAG = "SSCClient";
    private String sName;

    public SensorClientChannelService(String name) {
        super(name);
        sName = name;
    }

    @Override
    protected boolean handleIMMessage(IMMessage message, SSChannel channel) {
        SensorLog.i(sName, TAG, "client handleIMMessage, name=" + sName);
        SensorLog.i(sName, TAG, "handleIMMessage  type:" + message.getType());
        SensorLog.i(sName, TAG, "handleIMMessage  id: " + message.getId());
        SensorLog.i(sName, TAG, "handleIMMessage  content:" + message.getContent());
        SensorLog.i(sName, TAG, "handleIMMessage  source:" + message.getSource());
        SensorLog.i(sName, TAG, "handleIMMessage  target:" + message.getTarget());
        SensorLog.i(sName, TAG, "handleIMMessage  clientSource:" + message.getClientSource());
        SensorLog.i(sName, TAG, "handleIMMessage  clientTarget:" + message.getClientTarget());
        SensorLog.i(sName, TAG, "handleIMMessage  extra:" + message.getExtra());

        ChannelEvent event = new ChannelEvent(message.getClientTarget(), message.getClientSource(), message.getContent());
        event.mySession = message.getSource();
        event.targetSession = message.getTarget();
        EventBus.getDefault().post(event);
        return false;
    }
}
