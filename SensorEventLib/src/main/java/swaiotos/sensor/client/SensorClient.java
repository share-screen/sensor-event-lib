package swaiotos.sensor.client;

import android.content.Context;
import android.os.DeadObjectException;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import swaiotos.sensor.channel.ChannelMsgSender;
import swaiotos.sensor.channel.IMsgSender;
import swaiotos.sensor.channel.P2PMsgSender;
import swaiotos.sensor.client.data.ClientBusinessInfo;
import swaiotos.sensor.connect.IConnectCallback;
import swaiotos.sensor.data.AccountInfo;
import swaiotos.sensor.data.ChannelEvent;
import swaiotos.sensor.data.ClientCmdInfo;
import swaiotos.sensor.data.ServerCmdInfo;
import swaiotos.sensor.mgr.InfoManager;
import swaiotos.sensor.server.data.ServerInfo;
import swaiotos.sensor.touch.BarInputTouchView;
import swaiotos.sensor.touch.InputTouchView;
import swaiotos.sensor.utils.SensorLog;

/**
 * @Author: yuzhan
 */
public class SensorClient {

    private Context context;
    private IConnectClient client;
    private boolean bStart = false;
    protected IMsgSender sender;
    private InputTouchView view;
    private BarInputTouchView barView;
    private InfoManager infoManager;
    private ISmartApi smartApi;
    private P2PMsgSender p2PMsgSender;
    private boolean sensorEnable = false;
    private int sensorThreshold = 20;
    private String clientSSId = null;

    private static final String TAG = "SSCClient";

    public SensorClient(Context context, ClientBusinessInfo businessInfo, AccountInfo accountInfo) {
        clientSSId = businessInfo.clientSSId;
        SensorLog.setLogID(businessInfo.clientSSId);
        this.context = context;
        String id = accountInfo.mobile;//UUID.randomUUID().toString();
        SensorLog.d(clientSSId, TAG, "new SensorClient, accountInfo=" + accountInfo + ", businessInfo=" + businessInfo + ", id=" + id);

        if(businessInfo == null) {
            throw new RuntimeException("BusinessInfo must not be null.");
        }
        infoManager = new InfoManager();
        infoManager.setId(id);
        infoManager.setAccountInfo(accountInfo);
        infoManager.setBusinessInfo(businessInfo);

        client = new SensorConnectClient(infoManager);
        sender = new ChannelMsgSender(context, businessInfo.clientSSId);
        sender.setProtoVersion(businessInfo.protoVersion);
        ((ChannelMsgSender) sender).setAccountInfo(accountInfo);

        p2PMsgSender = new P2PMsgSender();
        p2PMsgSender.sender = sender;
        p2PMsgSender.targetId = businessInfo.targetSSId;
        p2PMsgSender.enableP2P = businessInfo.supportP2P;
        client.setP2PSender(p2PMsgSender);

        InfoManager.setAppContext(context);

        ClientCmdInfo.setBusinessInfo(businessInfo);
    }

    public void setOpenSimpleMode(boolean b){
        ((SensorConnectClient)client).setSimpleMotionEventData(b);
    }

    public void setSmartApi(ISmartApi smartApi) {
        this.smartApi = smartApi;
        client.setSmartApi(smartApi);
    }

    /**
     * 是否允许优先使用p2p
     * @param p2PFirst
     */
    public void setP2PFirst(boolean p2PFirst) {

    }

    public void refreshAccountInfo(AccountInfo accountInfo) {
        SensorLog.d(clientSSId, TAG, "refreshAccountInfo, accountInfo=" + accountInfo);
        infoManager.setAccountInfo(accountInfo);
    }

    public void sendMsgSticky(String content, String targetId) {
        try {
            sender.sendMsgSticky(content, targetId);
        } catch (DeadObjectException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return client.isConnected();
    }

    public void setShowTips(boolean showTips) {
        ((ChannelMsgSender) sender).setShowTips(showTips);
    }

    public void start() {
        SensorLog.d(clientSSId, TAG, "call start...");
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        bStart = true;
        try {
            ClientCmdInfo startCmd = ClientCmdInfo.build(infoManager, ClientCmdInfo.CMD_CLIENT_START);
            startCmd.setMsgP2p(sender.supportImP2P());
            sender.sendMsgSticky(JSON.toJSONString(startCmd), infoManager.getBusinessInfo().targetSSId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(view != null) {
            view.onStart();
        }
    }

    public void stop() {
        SensorLog.d(clientSSId, TAG, "call stop...");
        bStart = false;
//        sender.sendMsg(JSON.toJSONString(ClientCmdInfo.build(ClientCmdInfo.CMD_CLIENT_STOP)), InfoManager.getBusinessInfo().targetSSId);
        ClientCmdInfo cmdInfo = ClientCmdInfo.build(infoManager, ClientCmdInfo.CMD_CLIENT_STOP);
        cmdInfo.setMsgP2p(sender.supportImP2P());
        client.send(JSON.toJSONString(cmdInfo));
        client.disconnect();
        try {
            EventBus.getDefault().unregister(this);
        } catch (Exception e) {
//            e.printStackTrace();
        }
        if(view != null) {
            view.onStop();
        }
    }

    public View getView() {
        if(view == null) {
            view = new InputTouchView(context);
            view.setSize(infoManager.getBusinessInfo().width, infoManager.getBusinessInfo().height);
            view.setClient(client);
            view.setSensorEnable(sensorEnable);
            view.setSensorThreshold(sensorThreshold);
        }
        return view;
    }

    public View getBarView(int w, int h) {
        if(barView == null) {
            barView = new BarInputTouchView(context);
            barView.setSize(w, h);
            barView.setClient(client);
        }
        return barView;
    }

    public void setSensorEnable(boolean enable) {
        if(view != null) {
            view.setSensorEnable(enable);
        } else {
            sensorEnable = enable;
        }
    }

    public void setSensorThreshold(int t) {
        if(view != null) {
            view.setSensorThreshold(t);
        } else {
            sensorThreshold = t;
        }
    }

    public void resetSensorData() {
        ClientCmdInfo info = ClientCmdInfo.build(infoManager, ClientCmdInfo.CMD_CLIENT_SENSOR_RESET);
        info.setMsgP2p(p2PMsgSender.isP2P);
        client.send(JSON.toJSONString(info));
    }

    public void send(String data) {
        client.send(data);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ChannelEvent event) {
        SensorLog.d(clientSSId, TAG, "onEvent : " + event);
        if(event != null && !TextUtils.isEmpty(event.content)) {
            if(infoManager.getBusinessInfo().filterSource && !TextUtils.isEmpty(event.source)) {
                //检查是不是该业务的，如果不是，则不处理消息
                if(!TextUtils.equals(infoManager.getBusinessInfo().targetSSId, event.source)) {
                    Log.w(TAG, "not expect source : " + infoManager.getBusinessInfo().targetSSId + ", msg.source:" + event.source);
                    return ;
                }
            }
            if(infoManager.getBusinessInfo().filterClient && !TextUtils.isEmpty(event.client)) {
                //检查是不是该业务的，如果不是，则不处理消息
                if(!TextUtils.equals(infoManager.getBusinessInfo().clientSSId, event.client)) {
                    Log.w(TAG, "not expect client : " + infoManager.getBusinessInfo().clientSSId + ", msg.client:" + event.client);
                    return ;
                }
            }
            JSONObject jsonObject = JSON.parseObject(event.content);
            String cmd = jsonObject.getString("cmd");
            SensorLog.d(clientSSId, TAG, "cmd : " + cmd);
            if(ServerCmdInfo.CMD_SERVER_RECEIVE_CONNECT.equals(cmd)) {
                boolean isConnected = client.isConnected();
                SensorLog.d(clientSSId, TAG, "client isConnected=" + isConnected);
                if(!isConnected) {
                    try {
                        ServerCmdInfo info = JSON.parseObject(event.content, ServerCmdInfo.class);
                        SensorLog.d(clientSSId, TAG, "cId : " + info.cId + ", myId=" + infoManager.getId() + ", server use p2p=" + info.im_p2p);
                        if(TextUtils.equals(info.cId, infoManager.getId()) && !info.im_p2p) {
                            ServerInfo serverInfo = JSON.parseObject(info.content, ServerInfo.class);
                            SensorLog.d(clientSSId, TAG, "server cmd info=" + info + ", start connect : " + serverInfo.url);
                            client.connect(serverInfo.url, callback);
                        }
                    } catch (Exception e) {
                        SensorLog.d(clientSSId, TAG, "connect error" + e);
                        e.printStackTrace();
                    }
                }
                checkP2P(event);
            }
        }
    }
    
    private void checkP2P(ChannelEvent event) {
        boolean isP2P = false;
        if(event != null && event.mySession != null) {
            isP2P = event.mySession.supportP2P();
        }
        SensorLog.d(clientSSId, TAG, "checkP2P, isP2P=" + isP2P + ", session=" + event.mySession + ", targetSession=" + event.targetSession);
        p2PMsgSender.isP2P = isP2P;
    }

    public void connect(String url) {
        boolean isConnected = client.isConnected();
        SensorLog.d(clientSSId, TAG, "connect server : " + url + ", isConnected=" + isConnected);
        if(!isConnected) {
            client.connect(url, callback);
        } else {
            if(callback != null) {
                callback.onSuccess();
            }
        }
    }

    private IConnectCallback parentCallback;
    public void setCallback(IConnectCallback callback) {
        this.parentCallback = callback;
    }

    public IConnectCallback getCallback() {
        return parentCallback;
    }

    private IConnectCallback callback = new IConnectCallback() {
        @Override
        public void onSuccess() {
            SensorLog.d(clientSSId, TAG, "client connect onSuccess");
            ClientCmdInfo info = ClientCmdInfo.build(infoManager, ClientCmdInfo.CMD_CLIENT_CONNECT);
            info.setMsgP2p(sender.supportImP2P());
            client.send(JSON.toJSONString(info));
            if(parentCallback != null)
                parentCallback.onSuccess();
        }

        @Override
        public void onFail(String reason) {
            SensorLog.d(clientSSId, TAG, "client connect onFail : " + reason);
            if(parentCallback != null)
                parentCallback.onFail(reason);
        }

        @Override
        public void onFailOnce(String reason) {
            SensorLog.d(clientSSId, TAG, "onFailOnce : " + reason);
            if(parentCallback != null)
                parentCallback.onFailOnce(reason);
        }

        @Override
        public void onClose() {
            SensorLog.d(clientSSId, TAG, "client connect onClose");
            if(parentCallback != null)
                parentCallback.onClose();
        }

        @Override
        public void onMessage(String msg) {
            SensorLog.d(clientSSId, TAG, "client connect onMessage : " + msg);
            if(parentCallback != null)
                parentCallback.onMessage(msg);
        }
    };
}
