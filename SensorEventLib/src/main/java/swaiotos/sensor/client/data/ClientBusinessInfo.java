package swaiotos.sensor.client.data;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;


/**
 * @Author: yuzhan
 */
public class ClientBusinessInfo implements Serializable {

    /**
     * IOT-Channel对应的ID
     */
    public String clientSSId;

    /**
     * dongle端接收方ID
     */
    public String targetSSId;

    /**
     * 业务名称
     */
    public String businessName;

    /**
     * View的宽度
     */
    public int width;

    /**
     * View的高度
     */
    public int height;

    public transient int offsetX; //暂时无用
    public transient int offsetY; //暂时无用
    
    public int protoVersion = -1;

    //避免消息重复
    public transient boolean filterSource = false;
    //避免消息重复
    public transient boolean filterClient = false;

    public transient boolean supportP2P = false;

    public transient boolean imP2P = false;

    public ClientBusinessInfo() {

    }

    public ClientBusinessInfo(String client, String target, String name, int w, int h) {
        this.clientSSId = client;
        this.targetSSId = target;
        this.businessName = name;
        this.width = w;
        this.height = h;
    }

    public ClientBusinessInfo(String client, String target, String name, int w, int h, int oX, int oY) {
        this.clientSSId = client;
        this.targetSSId = target;
        this.businessName = name;
        this.width = w;
        this.height = h;
        this.offsetX = oX;
        this.offsetY = oY;
    }

    public ClientBusinessInfo filterSource(boolean f) {
        this.filterSource = f;

        return this;
    }

    public ClientBusinessInfo filterClient(boolean f) {
        this.filterClient = f;

        return this;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
