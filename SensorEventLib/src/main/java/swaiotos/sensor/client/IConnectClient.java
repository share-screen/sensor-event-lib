package swaiotos.sensor.client;

import android.hardware.SensorEvent;
import android.view.MotionEvent;
import android.view.View;

import java.util.Map;

import swaiotos.sensor.channel.IMsgSender;
import swaiotos.sensor.channel.P2PMsgSender;
import swaiotos.sensor.connect.IConnectCallback;

/**
 * @Author: yuzhan
 */
public interface IConnectClient {
    boolean isConnected();
    void connect(String url, IConnectCallback callback);
    void disconnect();
    void send(String text);
    void sendMotionEvent(MotionEvent event, View v);
    void sendMotionEvent(MotionEvent event, View v, boolean isBar);
    void sendMotionEvent(MotionEvent event, View v, Map<String, String> extra);
    void setSmartApi(ISmartApi smartApi);
    void sendSensorEvent(SensorEvent event, View v);
    void onSensorOrientationChanged(float x, float y, float z);
    void setScaleXY(float scaleX,float scaleY);
    void setP2PSender(P2PMsgSender sender);
}
