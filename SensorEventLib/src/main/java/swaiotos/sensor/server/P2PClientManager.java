package swaiotos.sensor.server;

import java.util.HashMap;
import java.util.Map;

import swaiotos.sensor.data.ClientCmdInfo;

/**
 * @Author: yuzhan
 */
public class P2PClientManager {

    private Map<String, ClientCmdInfo> clientMap = new HashMap<>();

    public void addP2PClient(String cid, ClientCmdInfo info) {
        clientMap.put(cid, info);
    }

    public void removeP2PClient(String cid) {
        clientMap.remove(cid);
    }

    public ClientCmdInfo getP2PClient(String cid) {
        return clientMap.get(cid);
    }

    public int size() {
        return clientMap.size();
    }
}
