package swaiotos.sensor.server;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;

import org.greenrobot.eventbus.EventBus;

import swaiotos.channel.iot.IOTAdminChannel;
import swaiotos.channel.iot.ss.SSAdminChannel;
import swaiotos.channel.iot.ss.channel.im.IMMessage;
import swaiotos.channel.iot.ss.client.ClientMsg;
import swaiotos.channel.iot.ss.session.Session;
import swaiotos.sensor.channel.ChannelMsgSender;
import swaiotos.sensor.data.ClientCmdInfo;
import swaiotos.sensor.server.data.ServerBusinessInfo;
import swaiotos.sensor.utils.SensorLog;

/**
 * @Author: yuzhan
 */
public class ServerChannelMsgSender extends ChannelMsgSender implements ClientMsg.ClientOnMsgListener {

    private boolean registerSuccess = false;
    private boolean p2pEnable = false;
    private ServerBusinessInfo businessInfo;

    private final static String TAG_P2P = "SSCServer-P2P";

    public ServerChannelMsgSender(Context context, String clientId) {
        super(context, clientId);
    }

    public ServerChannelMsgSender(Context context, ServerBusinessInfo businessInfo) {
        super(context, businessInfo.clientSSId);
        this.businessInfo = businessInfo;
    }

    @Override
    protected String logTag() {
        return "SSCServer";
    }

    @Override
    public void open() {
        registerSuccess = false;
        SensorLog.d(TAG, "bind iot-channel " + iotPkgName());
        IOTAdminChannel.mananger.open(context.getApplicationContext(), iotPkgName(), new IOTAdminChannel.OpenCallback() {
            @Override
            public void onConntected(SSAdminChannel channel) {
                SensorLog.d(TAG, "************  ServerChannelMsgSender init channel onConnected");
                ssChannel = channel;
                if(!TextUtils.isEmpty(stickyMsg)) {
                    try {
                        sendMsg(stickyMsg, stickyTargetId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                try {
                    SensorLog.d(TAG, "************  call addClientIDAndMsgListener, clientId=" + clientId);
                    channel.getClientMsg().addClientIDAndMsgListener(clientId, ServerChannelMsgSender.this);
                    if(businessInfo != null && businessInfo.compatSSIDList != null && !businessInfo.compatSSIDList.isEmpty()) {
                        for(String id : businessInfo.compatSSIDList) {
                            SensorLog.d(TAG, "************  call addClientIDAndMsgListener, compat clientId=" + id);
                            channel.getClientMsg().addClientIDAndMsgListener(id, ServerChannelMsgSender.this);
                        }
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String s) {
                SensorLog.d(TAG, "************ ServerChannelMsgSender init channel onError : " + s);
                ssChannel = null;
            }
        });
    }

    @Override
    public void sendMsg(String content, String targetId, boolean withProtoVersion) throws Exception {
        sendMsg(content, targetId, withProtoVersion, false);
    }

    @Override
    protected void sendMsg(String content, String targetId, boolean withProtoVersion, boolean intensive) throws Exception {
        try {
            SensorLog.d(TAG, "server sendBroadCast targetId : " + targetId + ", content : " + content);
            Session sourceSession = ssChannel.getSessionManager().getMySession();
//            Session targetSession = ssChannel.getSessionManager().getConnectedSession();
            SensorLog.d(TAG, "sourceSession=" + (sourceSession == null ? null : sourceSession.toString()));
            IMMessage message = new IMMessage.Builder()
                    .setBroadcast(true)
                    .setSource(sourceSession)
//                    .setTarget(targetSession)
                    .setClientSource(clientId)
                    .setClientTarget(targetId)
                    .setContent(content)
                    .setType(IMMessage.TYPE.TEXT)
                    .build();
//            message.setReqProtoVersion(0);
            ssChannel.getIMChannel().send(message, callback);
//            ssChannel.getIMChannel().send(message, callback);
        } catch (DeadObjectException e) {
            e.printStackTrace();
            SensorLog.d(TAG, "DeadObjectException, re open now.");
            open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean supportImP2P() {
        if(ssChannel != null) {
            try {
                return ssChannel.getSessionManager().getMySession().supportP2P();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    protected String iotPkgName() {
        return "swaiotos.channel.iot";
    }

    @Override
    public void onRegisterSuccess() {
        SensorLog.d(TAG, "onRegisterSuccess");
        registerSuccess = true;
        ServerConfig.setAIDLChannel(true);
    }

    public boolean isRegisterSuccess() {
        return registerSuccess;
    }

    public void setP2PEnable(boolean b) {
        p2pEnable = b;
    }

    //文档经过了转发处理
    private boolean isDispatchTarget(String clientTarget, String expectClientSSID) {
        if("ss-doc-control-client".equals(expectClientSSID) && "ss-clientID-UniversalMediaPlayer".equals(clientTarget)) {
            return true;
        }
        return businessInfo != null && businessInfo.isCompatSSID(clientTarget);
    }

    @Override
    public boolean onClientMsg(IMMessage message) {
        if(!p2pEnable) return false;//需要p2p enable时候才使用
        if(!TextUtils.equals(message.getClientTarget(), clientId) && !isDispatchTarget(message.getClientTarget(), clientId)) {
            SensorLog.i(TAG_P2P, "server onClientMsg not expect!! clientTarget:" + message.getClientTarget() + ", expect clientSSID=" + clientId);
            return false;
        }
        SensorLog.i(TAG_P2P, "server onClientMsg  type:" + message.getType());
        SensorLog.i(TAG_P2P, "server onClientMsg  id: " + message.getId());
        SensorLog.i(TAG_P2P, "server onClientMsg  content:" + message.getContent());
        SensorLog.i(TAG_P2P, "server onClientMsg  source:" + message.getSource());
        SensorLog.i(TAG_P2P, "server onClientMsg  target:" + message.getTarget() + ", expect_ssid=" + clientId);
        SensorLog.i(TAG_P2P, "server onClientMsg  clientSource:" + message.getClientSource());
        SensorLog.i(TAG_P2P, "server onClientMsg  clientTarget:" + message.getClientTarget());
        SensorLog.i(TAG_P2P, "server onClientMsg  extra:" + message.encode());

        ClientCmdInfo info = JSON.parseObject(message.getContent(), ClientCmdInfo.class);
        if(ClientCmdInfo.CMD_CLIENT_START.equals(info.cmd)) {
            MessageEventData eventData = new MessageEventData();
            eventData.message = message;
            EventBus.getDefault().post(eventData);
        } else if(ClientCmdInfo.CMD_CLIENT_STOP.equals(info.cmd)) {
            MessageEventData eventData = new MessageEventData();
            eventData.message = message;
            EventBus.getDefault().post(eventData);
        } else if(ClientCmdInfo.CMD_CLIENT_MOTION_EVENT.equals(info.cmd)) {
            SensorLog.d(TAG_P2P, "server onClientMsg receive client motion event, cid=" + info.cid);
            info.setMsgP2p(message.getSource().supportP2P());
            EventBus.getDefault().post(info);
        } else if(ClientCmdInfo.CMD_CLIENT_SENSOR_EVENT.equals(info.cmd)) {
            SensorLog.d(TAG_P2P, "server onClientMsg receive client sensor event, cid=" + info.cid);
            info.setMsgP2p(message.getSource().supportP2P());
            EventBus.getDefault().post(info);
        }
        return p2pEnable;
    }
}
