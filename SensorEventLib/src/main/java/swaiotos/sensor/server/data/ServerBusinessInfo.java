package swaiotos.sensor.server.data;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * @Author: yuzhan
 */
public class ServerBusinessInfo implements Serializable {

    /**
     * IOT-Channel对应的ID
     */
    public String clientSSId;


    /**
     * 业务名称
     */
    public String businessName;

    public Set<String> compatSSIDList;

    public ServerBusinessInfo() {

    }

    public ServerBusinessInfo(String client, String name) {
        this.clientSSId = client;
        this.businessName = name;
    }

    /**
     * 部分业务会经过中间中转，增加clientSSID兼容逻辑
     * @return
     */
    public ServerBusinessInfo addCompatClientSSId(String id) {
        if(TextUtils.isEmpty(id) || TextUtils.equals(clientSSId, id)) {
            return this;
        }
        if(compatSSIDList == null) {
            compatSSIDList = new HashSet<>();
        }
        compatSSIDList.add(id);

        return this;
    }

    public boolean isCompatSSID(String id) {
        return compatSSIDList != null && compatSSIDList.contains(id);
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
