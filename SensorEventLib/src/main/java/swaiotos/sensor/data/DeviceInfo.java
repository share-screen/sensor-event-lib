package swaiotos.sensor.data;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;


/**
 * @Author: yuzhan
 */
public class DeviceInfo implements Serializable {

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
