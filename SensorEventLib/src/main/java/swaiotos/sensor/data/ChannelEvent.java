package swaiotos.sensor.data;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;

import swaiotos.channel.iot.ss.session.Session;

/**
 * @Author: yuzhan
 */
public class ChannelEvent implements Serializable {
    public String content;
    public String source;
    public String client;
    public Session mySession;
    public Session targetSession;

    public ChannelEvent() {

    }

    public ChannelEvent(String client, String source, String c) {
        this.client = client;
        this.source = source;
        this.content = c;
    }

    public ChannelEvent(String c) {
        this.content = c;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
