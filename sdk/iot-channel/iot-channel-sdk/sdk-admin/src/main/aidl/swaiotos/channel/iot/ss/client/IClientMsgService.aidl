// IControllerService.aidl
package swaiotos.channel.iot.ss.client;

// Declare any non-default types here with import statements
import swaiotos.channel.iot.ss.client.IBaseClientOnMsgListener;

interface IClientMsgService {
    // 添加消息回调监听器
        void addClientIDAndMsgListener(in String clientID,in IBaseClientOnMsgListener listener);
    // 移除客户端消息回调监听器
        void removeClientIDAndMsgListener(in String clientID,in IBaseClientOnMsgListener listener);
}
