// IBindResult.aidl
package swaiotos.channel.iot.ss.client;

// Declare any non-default types here with import statements
import swaiotos.channel.iot.ss.channel.im.IMMessage;
// 客户端消息回调
interface IBaseClientOnMsgListener {
    void onRegisterSuccess();
    void callbackClientMsg(in IMMessage msg);
}
