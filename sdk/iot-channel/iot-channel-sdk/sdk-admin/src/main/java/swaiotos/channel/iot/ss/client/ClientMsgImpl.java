package swaiotos.channel.iot.ss.client;

import android.os.RemoteException;

import java.util.LinkedHashMap;
import java.util.Map;

import swaiotos.channel.iot.ss.SSChannel;
import swaiotos.channel.iot.ss.channel.im.IMMessage;
import swaiotos.channel.iot.ss.controller.IControllerService;
import swaiotos.channel.iot.ss.device.Device;
import swaiotos.channel.iot.ss.device.DeviceManager;
import swaiotos.channel.iot.ss.device.IOnDeviceChangedListener;
import swaiotos.channel.iot.utils.ThreadManager;

/**
 * @Package: swaiotos.channel.iot.ss.client
 * @ClassName: ClientMsgImpl
 * @Description: java类作用描述
 * @Author: wesly.ue
 * @CreateDate: 2021/8/10 11:20
 * @UpdateUser: 更新者
 * @UpdateDate: 2021/8/10 11:20
 * @UpdateRemark: 更新说明
 * @Version: 1.0
 */
public class ClientMsgImpl implements ClientMsg, SSChannel.IClient<IClientMsgService> {
    private IClientMsgService service;
    private final Map<String, IBaseClientOnMsgListener> msgServiceMap = new LinkedHashMap<>();

    @Override
    public void setService(IClientMsgService service) {
        this.service = service;
    }

    @Override
    public IClientMsgService getService() {
        return service;
    }

    @Override
    public void addClientIDAndMsgListener(final String clientID,final ClientOnMsgListener listener) throws RemoteException {
        synchronized (msgServiceMap) {
            if (!msgServiceMap.containsKey(clientID)) {
                IBaseClientOnMsgListener l = new IBaseClientOnMsgListener.Stub() {

                    @Override
                    public void onRegisterSuccess() throws RemoteException {
                        ThreadManager.getInstance().ioThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    listener.onRegisterSuccess();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }

                    @Override
                    public void callbackClientMsg(final IMMessage msg) throws RemoteException {
                        ThreadManager.getInstance().ioThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    listener.onClientMsg(msg);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                };
                msgServiceMap.put(clientID, l);
                service.addClientIDAndMsgListener(clientID,l);
            }
        }
    }

    @Override
    public void removeClientIDAndMsgListener(final String clientID,final ClientOnMsgListener listener) throws RemoteException {
        synchronized (msgServiceMap) {
            IBaseClientOnMsgListener l = msgServiceMap.get(clientID);
            if (l != null) {
                service.removeClientIDAndMsgListener(clientID,l);
                msgServiceMap.remove(clientID);
            }
        }
    }
}
