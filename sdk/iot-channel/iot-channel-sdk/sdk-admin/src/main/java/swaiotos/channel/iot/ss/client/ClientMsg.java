package swaiotos.channel.iot.ss.client;

import android.os.RemoteException;

import swaiotos.channel.iot.ss.channel.im.IMMessage;

/**
 * @ProjectName: SmartLife
 * @Package: swaiotos.channel.iot.ss.client
 * @ClassName: ClientMsg
 * @Description: java类作用描述
 * @Author: wangyuehui
 * @CreateDate: 2021/8/10 11:13
 * @UpdateUser: 更新者
 * @UpdateDate: 2021/8/10 11:13
 * @UpdateRemark: 更新说明
 * @Version: 1.0
 */
public interface ClientMsg {
    interface ClientOnMsgListener {
        void onRegisterSuccess();
        boolean onClientMsg(IMMessage immessage);
    }
    // 添加消息回调监听器
    void addClientIDAndMsgListener(String clientID,ClientOnMsgListener listener) throws RemoteException;
    // 移除客户端消息回调监听器
    void removeClientIDAndMsgListener(String clientID, ClientOnMsgListener listener) throws RemoteException;
}
