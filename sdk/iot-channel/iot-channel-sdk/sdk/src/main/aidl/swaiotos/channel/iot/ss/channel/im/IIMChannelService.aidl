// IIMChannelService.aidl
package swaiotos.channel.iot.ss.channel.im;

// Declare any non-default types here with import statements

import swaiotos.channel.iot.ss.channel.im.IMMessage;
import swaiotos.channel.iot.ss.session.Session;
import swaiotos.channel.iot.utils.ipc.ParcelableObject;

// （重要服务）IIMChannelService IM 通道接口服务，IOT-Service需要实现该接口
interface IIMChannelService {
// 发送接口，传入adroid.os.Messenger的对象及message对象
    void send(in IMMessage message,in Messenger callback);
// 同步发送接口，回复aidl对象
    ParcelableObject sendSync(in IMMessage message,in Messenger callback,long timeout);
// 重新设置sid和token接口（比如重新登陆账号）
    void reset(in String sid, in String token);
// 发送广播接口 （与send接口重复设计）
    void sendBroadCast(in IMMessage message,in Messenger callback);
// 文件服务
    String fileService(in String path);
// 重新设置sid，token和userid
    void resetSidAndUserId(in String sid, in String token, in String userId);
// 重新设置环境
    void resetEvn(in String sid, in String token, in String userId,in String evn,in Map map);

    void sendBle(in String message);

    void setBleCallBack(in Messenger callback);
}
