// IMainService.aidl
package swaiotos.channel.iot.ss;

// Declare any non-default types here with import statements

import swaiotos.channel.iot.utils.ipc.ParcelableBinder;

// Main服务
interface IMainService {
    // 根据packageName 打开服务
    ParcelableBinder open(String packageName);
}
