package swaiotos.channel.iot.ss.channel.stream;

// 流通道服务接收类
interface IStreamChannelReceiver{
// onReceive 接收接口
 void onReceive(in byte[] data);
}