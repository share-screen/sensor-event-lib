// IBindResult.aidl
package swaiotos.channel.iot.ss.device;

// Declare any non-default types here with import statements

// 临时设备状态监听
interface IBaseTempDeviceStatusListener {
// 设备加入
     void handleJoin(String sid);
// 设备离开
     void handleLeave(String sid);
}
