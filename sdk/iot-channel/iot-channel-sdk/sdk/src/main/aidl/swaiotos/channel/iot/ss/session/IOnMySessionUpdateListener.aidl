// IOnMySessionUpdateListener.aidl
package swaiotos.channel.iot.ss.session;

// Declare any non-default types here with import statements

import swaiotos.channel.iot.ss.session.Session;

// 会话管理监听
interface IOnMySessionUpdateListener {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
     // 会话更新回调
    void onMySessionUpdate(in Session mySession);
}
