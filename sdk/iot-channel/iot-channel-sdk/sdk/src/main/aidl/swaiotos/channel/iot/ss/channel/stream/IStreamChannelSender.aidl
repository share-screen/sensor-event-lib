// IStreamChannelSender.aidl
package swaiotos.channel.iot.ss.channel.stream;

// Declare any non-default types here with import statements
import swaiotos.channel.iot.ss.channel.stream.IStreamChannelSenderMonitor;

// 流通道服务发送接口
interface IStreamChannelSender {
// 发送data数据
    void send(in byte[] data);
// 是否可用
    boolean available();
// 设置流通道监控对象
    void setSenderMonitor(in IStreamChannelSenderMonitor monitor);
}
