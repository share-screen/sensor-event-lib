// IBindResult.aidl
package swaiotos.channel.iot.ss.device;

// Declare any non-default types here with import statements
import swaiotos.channel.iot.ss.device.Device;
// 基础设备信息更新监听
interface IBaseDeviceInfoUpdateListener {
// 设备信息更新
     void onDeviceInfoUpdate(in List<Device> devices);
// SSE登陆成功
     void sseLoginSuccess();
// 登陆状态
     void loginState(int code,String info);
// 登陆连接状态（）
     void loginConnectingState(int code,String info);
}
