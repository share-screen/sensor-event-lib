// ISSChannelService.aidl
 package swaiotos.channel.iot.ss;

 // Declare any non-default types here with import statements

 import swaiotos.channel.iot.ss.session.ISessionManagerService;
 import swaiotos.channel.iot.ss.channel.im.IIMChannelService;
 import swaiotos.channel.iot.ss.channel.stream.IStreamChannelService;
 import swaiotos.channel.iot.ss.device.IDeviceManagerService;

// 通道服务
 interface ISSChannelService {
 // 获得会话管理的服务
     ISessionManagerService getSessionManager();
 // 获得IM通道服务
     IIMChannelService getIMChannel();
 // 获得流传输通道的服务
     IStreamChannelService getStreamChannel();
 // 获得设备管理服务
     IDeviceManagerService getDeviceManager();
 // 获得binder接口
     IBinder getBinder(String name);
 }
