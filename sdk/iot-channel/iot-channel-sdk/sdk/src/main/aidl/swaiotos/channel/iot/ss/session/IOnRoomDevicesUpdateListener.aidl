// IBindResult.aidl
package swaiotos.channel.iot.ss.session;

// Declare any non-default types here with import statements

// 房间设备数量更新
interface IOnRoomDevicesUpdateListener {
// 房间设备数量更新回调
    void onRoomDevicesUpdate(in int count);
}
