package swaiotos.channel.iot.ss.channel.stream;

import swaiotos.channel.iot.ss.channel.stream.IStreamChannelReceiver;
import swaiotos.channel.iot.ss.session.Session;
import swaiotos.channel.iot.ss.channel.stream.IStreamChannelSender;
// 流通道服务类
interface IStreamChannelService {
// 打开流通道服务
   int open(in IStreamChannelReceiver stream);
// 关闭流通道服务，channelId用于管理
   void close(int channelId);
// 打开Sender对象，包含一个session和channelid，返回IStreamChannelSender
   IStreamChannelSender openSender(in Session session ,int channelId);
// 关闭IStreamChannelSender对象
   void closeSender(IStreamChannelSender sender);
}
