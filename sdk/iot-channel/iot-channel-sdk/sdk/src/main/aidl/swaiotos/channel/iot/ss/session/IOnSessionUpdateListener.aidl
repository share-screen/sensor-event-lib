// IBindResult.aidl
package swaiotos.channel.iot.ss.session;

// Declare any non-default types here with import statements

import swaiotos.channel.iot.ss.session.Session;

// 会话更新监听
interface IOnSessionUpdateListener {
// 会话连接
    void onSessionConnect(in Session session);
// 会话刷新
    void onSessionUpdate(in Session session);
// 会话断开连接
    void onSessionDisconnect(in Session session);
}
