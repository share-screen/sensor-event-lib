// IBindResult.aidl
package swaiotos.channel.iot.ss.device;

// Declare any non-default types here with import statements

// 设备关系监听
interface IDeviceRelationListener {
// 设备绑定回调
     void onDeviceBind(String lsid);
// 设备解绑回调
     void onDeviceUnbind(String lsid);
}
