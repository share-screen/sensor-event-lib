// ISessionManagerService.aidl
package swaiotos.channel.iot.ss.session;

// Declare any non-default types here with import statements

import swaiotos.channel.iot.ss.session.Session;
import swaiotos.channel.iot.ss.session.IOnSessionUpdateListener;
import swaiotos.channel.iot.ss.session.IOnMySessionUpdateListener;
import swaiotos.channel.iot.utils.ipc.ParcelableObject;
import swaiotos.channel.iot.ss.session.RoomDevice;
import swaiotos.channel.iot.ss.session.IOnRoomDevicesUpdateListener;

// （重要服务）会话管理服务
interface ISessionManagerService {
    /**
     * Add on my session update listener.
     *
     * @param listener the listener
     * @throws Exception the exception
     */
     // 添加会话更新的回调
    void addOnMySessionUpdateListener(in IOnMySessionUpdateListener listener);

    /**
     * Remove on my session update listener.
     *
     * @param listener the listener
     * @throws Exception the exception
     */
     // 移除会话更新的回调
    void removeOnMySessionUpdateListener(in IOnMySessionUpdateListener listener);

    /**
     * 获取当前设备的Session
     *
     * @return the my session
     * @throws Exception the exception
     */
     // 获取当前会话
    ParcelableObject getMySession();

    /**
     * Add on session connect update listener.
     *
     * @param listener the listener
     * @throws Exception the exception
     */
     // 添加连接会话刷新的回调
    void addConnectedSessionOnUpdateListener(in IOnSessionUpdateListener listener);

    /**
     * Remove on session connect update listener.
     *
     * @param listener the listener
     * @throws Exception the exception
     */
     // 移除连接会话刷新的回调
    void removeConnectedSessionOnUpdateListener(in IOnSessionUpdateListener listener);

    /**
     * Gets the device Session for the current device connection
     *
     * @return the connected session
     * @throws Exception the exception
     */
     // 获得连接会话
    ParcelableObject getConnectedSession();

    /**
     * Set the connect Session state monitor
     *
     * @param listener the listener
     * @throws Exception the exception
     */
     // 添加服务端会话更新的回调
    void addServerSessionOnUpdateListener(in IOnSessionUpdateListener listener);

    /**
     * Remove on session update listener.
     *
     * @param listener the listener
     * @throws Exception the exception
     */
     // 移除服务端会话更新的回调
    void removeServerSessionOnUpdateListener(in IOnSessionUpdateListener listener);

    /**
     * Gets the Session list of the current device connected
     *
     * @return the server sessions
     * @throws Exception the exception
     */
     // 获得服务端会话
    List<Session> getServerSessions();

    // 判断会话是否可用
    boolean available(in Session session,String channel);

    // 是否连接上SSE
    boolean isConnectSSE();

    /**
      * Gets the Device of connection  to the current room
      *
      * @return the server room of device
       * @throws Exception the exception
    */
    // 获得房间列表
    List<RoomDevice> getRoomDevices();

    /**
    * Listen for changes in the number of room connections
    *
    */
    // 添加房间设备更新的回调
    void addRoomDevicesOnUpdateListener(in IOnRoomDevicesUpdateListener listener);
    // 移除房间设备更新的回调
    void removeRoomDevicesOnUpdateListener(in IOnRoomDevicesUpdateListener listener);
    // 清理连接的会话
    void clearConnectedSessionByUser();
}
