// IBindResult.aidl
package swaiotos.channel.iot.ss.device;

// Declare any non-default types here with import statements
import swaiotos.channel.iot.ss.device.Device;

// 设备刷新监听
interface IBaseDevicesReflushListener {
// 设备flush更新
     void onDeviceReflushUpdate(in List<Device> devices);
}
