// IBindResult.aidl
package swaiotos.channel.iot.ss.device;

// Declare any non-default types here with import statements

import swaiotos.channel.iot.ss.device.Device;

// 设备变化监听
interface IBaseOnDeviceChangedListener {
// 设备离线消息监听
     void onDeviceOffLine(in Device device);
// 设备在线消息监听
     void onDeviceOnLine(in Device device);
// 设备更新消息监听
     void onDeviceUpdate(in Device device);
}
