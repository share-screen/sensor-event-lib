// IStreamChannelSender.aidl
package swaiotos.channel.iot.ss.channel.stream;

// Declare any non-default types here with import statements

// 发送通道监听类
interface IStreamChannelSenderMonitor {
// 是否可用状态改变
    void onAvailableChanged(boolean available);
}
