// IbindDevices.aidl
package swaiotos.channel.iot.ss.device;

import swaiotos.channel.iot.ss.device.Device;
import swaiotos.channel.iot.ss.device.IBaseOnDeviceChangedListener;
import swaiotos.channel.iot.ss.device.IDeviceRelationListener;
import swaiotos.channel.iot.ss.device.IBaseDeviceInfoUpdateListener;
import swaiotos.channel.iot.ss.device.IBaseDevicesReflushListener;
import swaiotos.channel.iot.ss.device.IBaseTempDeviceStatusListener;

// 设备管理服务
interface IDeviceManagerService {
// 获取设备列表
    List<Device> getDevices();
// 获取设备在线状态
    List<Device> getDeviceOnlineStatus();
// 获得当前设备信息
    Device getCurrentDevice();
// 添加设备变化监听类
    void addOnDeviceChangedListener(in IBaseOnDeviceChangedListener listener);
// 移除设备变化监听类
    void removeOnDeviceChangedListener(in IBaseOnDeviceChangedListener listener);
// 添加设备绑定监听类
    void addDeviceBindListener(in IDeviceRelationListener listener);
// 移除设备绑定监听类
    void removeDeviceBindListener(in IDeviceRelationListener listener);
// 添加设备更新监听类
    void addDeviceInfoUpdateListener(in IBaseDeviceInfoUpdateListener listener);
// 移除设备更新监听类
    void removeDeviceInfoUpdateListener(in IBaseDeviceInfoUpdateListener listener);
// 添加设备刷新监听类
    void addDevicesReflushListener(in IBaseDevicesReflushListener listener);
// 移除设备刷新监听类
    void removeDevicesReflushListener(in IBaseDevicesReflushListener listener);
// 更新设备列表
    List<Device> updateDeviceList();
// 获得ak信息
    String getAccessToken();

}
