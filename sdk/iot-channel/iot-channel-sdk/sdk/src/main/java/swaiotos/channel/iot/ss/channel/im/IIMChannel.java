package swaiotos.channel.iot.ss.channel.im;

import java.util.Map;

/**
 * @ClassName: IIMChannel
 * @Author: lu
 * @CreateDate: 2020/4/1 8:34 PM
 * @Description:
 */
// IM通道接口
public interface IIMChannel extends IIMChannelCore {
    // 同步发送接口
    IMMessage sendSync(IMMessage message, IMMessageCallback callback, long timeout) throws Exception;
    // 同步发送接口+timeout
    IMMessage sendSync(IMMessage message, long timeout) throws Exception;
    // 重置接口
    void reset(String sid, String token) throws Exception;
    // 重置接口
    void reset(String sid, String token, String userId) throws Exception;
    // 发送广播接口
    void sendBroadCast(IMMessage message, IMMessageCallback callback) throws Exception;
    // 文件路径
    String fileService(String path) throws Exception;

    /**
     * 环境
     * evn: Production(正式环境)、Beta（测试环境） Dev（研发环境）不需要传map  其他环境传map
     * */
    void resetEvn(String sid, String token, String userId,String evn, Map<String,String> configMap);
}
