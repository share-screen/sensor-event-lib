package swaiotos.channel.iot.ss.device;

import android.os.RemoteException;

import java.util.List;

import swaiotos.channel.iot.ss.session.Session;

/**
 * @ClassName: Devices
 * @Author: colin
 * @CreateDate: 2020/4/15 19:48 PM
 * @Description:
 */
public interface DeviceManager {
    /**
     * 设备列表查询
     *
     * @return List<Device> 返回所有绑定的设备
     * @throws Exception the exception
     */
    List<Device> getDevices() throws Exception;

    /**
     * 设备列表查询 并实时返回设备在线状态
     */
    List<Device> getDeviceOnlineStatus() throws Exception;


    /**
     * @return Device：设备信息
     * @throws Exception
     */
    Device getCurrentDevice() throws Exception;


    /**
     * @param sid
     * @return
     * @throws Exception
     */
    Session getLocalSessionBySid(String sid) throws Exception;

    /**
     * 设备属性变化
     */
    interface OnDeviceChangedListener {
        // 设备离线
        void onDeviceOffLine(Device device);

        // 设备上线
        void onDeviceOnLine(Device device);

        // 设备更新
        void onDeviceUpdate(Device device);
    }

    // 添加设备变化监听类
    void addOnDeviceChangedListener(OnDeviceChangedListener listener) throws RemoteException;

    // 移除设备变化监听类
    void removeOnDeviceChangedListener(OnDeviceChangedListener listener) throws RemoteException;

    // 设备绑定监听类
    interface OnDeviceBindListener {
        // 设备绑上了
        void onDeviceBind(String lsid);

        // 设备被解绑了
        void onDeviceUnBind(String lsid);
    }

    /**
     * 设备网络请求监听
     */
    interface OnDevicesReflushListener {
        // 设备变化刷新
        void onDeviceReflushUpdate(List<Device> devices);
    }

    // 添加设备绑定监听回调对象
    void addDeviceBindListener(OnDeviceBindListener listener) throws RemoteException;

    // 移除设备绑定监听回调对象
    void removeDeviceBindListener(OnDeviceBindListener listener) throws RemoteException;

    // 设备信息刷新监听对象
    interface OnDeviceInfoUpdateListener {
        // 设备信息更新
        void onDeviceInfoUpdate(List<Device> devices);
        // sse登陆成功
        void sseLoginSuccess();
        // 登陆状态
        void loginState(int code, String info);
        // 登陆状态
        void loginConnectingState(int code,String info);
    }

    void addDeviceInfoUpdateListener(OnDeviceInfoUpdateListener listener) throws RemoteException;

    void removeDeviceInfoUpdateListener(OnDeviceInfoUpdateListener listener) throws RemoteException;

    void addDevicesReflushListener(OnDevicesReflushListener listener) throws RemoteException;

    void removeDevicesReflushListener(OnDevicesReflushListener listener) throws RemoteException;

    /**
     * 同步网络请求拉取数据
     **/
    List<Device> updateDeviceList();

    // 获得accessToken
    String getAccessToken() throws RemoteException;

}
