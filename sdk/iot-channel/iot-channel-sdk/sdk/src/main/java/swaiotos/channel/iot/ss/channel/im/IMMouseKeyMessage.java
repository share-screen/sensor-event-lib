//package swaiotos.channel.iot.ss.channel.im;
//
//import org.json.JSONObject
//
///**
// * @desc : 用于显示、隐藏、移动鼠标
// * @author : yiBao
// * @date : 2021/7/28
// **/
//class IMMouseKeyMessage : IMMessage() {
//    var showMouse: Boolean = false
//    var x: Int = 0
//    var y: Int = 0
//    var width: Int = 0
//    var height: Int = 0
//    var screenWidth: Int = 0
//    var screenHeight: Int = 0
//
//    companion object {
//        fun decode(body: String): IMMouseKeyMessage {
//            // todo 寻找json工具库来优化
//            val mouseMessage = IMMouseKeyMessage()
//            val jbt = JSONObject(body)
//            mouseMessage.showMouse = jbt.getBoolean("showMouse")
//            mouseMessage.x = jbt.getInt("x")
//            mouseMessage.y = jbt.getInt("y")
//            mouseMessage.width = jbt.getInt("width")
//            mouseMessage.height = jbt.getInt("height")
//            mouseMessage.screenWidth = jbt.getInt("screenWidth")
//            mouseMessage.screenHeight = jbt.getInt("screenHeight")
//            return mouseMessage
//        }
//    }
//}
//
