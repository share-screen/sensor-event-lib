package swaiotos.channel.iot.ss.device;

import java.util.List;

import swaiotos.channel.iot.ss.SSChannel;

/**
 * @ClassName: IDeviceManagerClient
 * @Author: lu
 * @CreateDate: 2020/4/18 5:14 PM
 * @Description:
 */
public interface DeviceManagerClient extends DeviceManager, SSChannel.IClient<IDeviceManagerService> {

    /**异步拉取网络数据*/
    interface DeviceListCallBack {
        // 设备列表变化接口
        void onDevices(List<Device> list);
    }

    // 更新设备列表变化接口
    void updateDeviceList(DeviceListCallBack callBack);

    // 停用接口
    void close();

}
