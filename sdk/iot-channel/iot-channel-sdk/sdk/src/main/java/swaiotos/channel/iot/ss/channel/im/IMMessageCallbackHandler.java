package swaiotos.channel.iot.ss.channel.im;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;

import java.util.LinkedHashMap;
import java.util.Map;

import swaiotos.channel.iot.utils.BaseSDKLogUtil;

/**
 * @ClassName: IMMessageCallback
 * @Author: lu
 * @CreateDate: 2020/4/1 6:47 PM
 * @Description:
 */
final class IMMessageCallbackHandler extends Handler {
    // android.os.Messenger ipc跨进程消息
    private Messenger mMessenger;

    // 消息map，消息key，消息回调方
    private Map<IMMessage, IMMessageCallback> mCallbacks = new LinkedHashMap<>();

    // 构造函数主loop
    public IMMessageCallbackHandler() {
        this(Looper.getMainLooper());
    }

    // 构造函数主looper
    public IMMessageCallbackHandler(Looper looper) {
        super(looper);
        mMessenger = new Messenger(this);
    }

    // 增加callback
    final void add(IMMessage message, IMMessageCallback callback) {
        synchronized (mCallbacks) {
            if (!mCallbacks.containsKey(message)) {
                mCallbacks.put(message, callback);
            }
        }
    }

    // 增加了handleMessage的时候，没有查到callback的打印
    @Override
    public final void handleMessage(Message msg) {
        super.handleMessage(msg);
        int method = IMMessageCallback.Builder.parseMethod(msg);
        IMMessage message = IMMessageCallback.Builder.parseIMMessage(msg);
        if (message != null) {
            IMMessageCallback callback;
            synchronized (mCallbacks) {
                callback = mCallbacks.get(message);
            }
            if (callback == null) {
                BaseSDKLogUtil.e("Thread name = " + this.getLooper().getThread().getName() + " handle IMMessage callback ,msg.what = " + msg.what +" " +
                        "but callback is null!");
                return;
            }
            switch (method) {
                case 0: {
                    callback.onStart(message);
                    break;
                }
                case 1: {
                    int progress = IMMessageCallback.Builder.parseProgress(msg);
                    callback.onProgress(message, progress);
                    break;
                }
                case 2: {
                    int code = IMMessageCallback.Builder.parseCode(msg);
                    String info = IMMessageCallback.Builder.parseInfo(msg);
                    callback.onEnd(message, code, info);
                    break;
                }
                default:
                    break;
            }
        }
    }

    // 获得android.os.Messenger
    final Messenger getMessenger() {
        return mMessenger;
    }
}
