package swaiotos.channel.iot.ss.channel.stream;

import swaiotos.channel.iot.ss.channel.im.IMMessage;
import swaiotos.channel.iot.ss.channel.im.IMMessageCallback;
import swaiotos.channel.iot.ss.session.Session;

// 流通道
public interface IStreamChannel {
    // 流数据接收监听类
    interface Receiver {
        // 收到流数据
        void onReceive(byte[] data);
        // 终端关闭了
        void closeReceiveClient(String sid);
    }

    // 流发送监控类
    interface SenderMonitor {
        // 流发送变得可用
        void onAvailableChanged(boolean available);
    }

    // 流发送类
    interface Sender {
        // 设置发送监控类
        void setSenderMonitor(SenderMonitor monitor);

        // 是否可用
        boolean available();

        // 发送接口
        void send(IMMessage msg, IMMessageCallback callback) throws Exception;

        // 发送接口
        void send(String message) throws  Exception;

        // 本地是否连接
        void localConnected(boolean success);

        // 重新打开
        void reOpenClient(String ip);
    }

    int openReceiver(Receiver receiver);

    void closeReceiver(int channelId);

    Sender openSender(Session session, int channelId);

    void closeSender(Sender sender);
}
