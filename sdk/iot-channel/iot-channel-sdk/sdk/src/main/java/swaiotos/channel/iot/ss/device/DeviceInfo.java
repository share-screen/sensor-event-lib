package swaiotos.channel.iot.ss.device;

import android.os.Parcelable;

/**
 * @ClassName: DeviceInfo
 * @Author: lu
 * @CreateDate: 2020/4/22 2:43 PM
 * @Description:
 */
public abstract class DeviceInfo implements Parcelable {
    // 设备枚举类型
    public enum TYPE {
        TV,
        PAD,
        PHONE,
        THIRD
    }

    // 类名
    public final String clazzName = getClass().getName();

    // 设备类型
    public abstract TYPE type();

    // 提供encode方法
    public abstract String encode();
}
