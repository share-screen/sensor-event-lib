package swaiotos.channel.iot.ss.channel.im;

/**
 * @ClassName: IIMChannelCore
 * @Author: lu
 * @CreateDate: 2020/4/13 2:25 PM
 * @Description:
 */
// IM通道核心
public interface IIMChannelCore {
    // 异步发送接口
    void send(IMMessage message, IMMessageCallback callback) throws Exception;
    // 发送接口
    void send(IMMessage message) throws Exception;

    void sendBle(String message);
}
