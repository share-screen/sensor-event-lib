package swaiotos.channel.iot.ss.server.data.log;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.skyworth.framework.skysdk.properties.SkySystemProperties;

import java.lang.reflect.Method;

import swaiotos.channel.iot.ss.analysis.UserBehaviorAnalysis;
import swaiotos.channel.iot.ss.server.utils.Constants;
import swaiotos.channel.iot.ss.server.utils.MACUtils;
import swaiotos.channel.iot.utils.AndroidLog;
import swaiotos.sal.SAL;
import swaiotos.sal.SalModule;
import swaiotos.sal.platform.IDeviceInfo;
import swaiotos.sal.platform.ISystemInfo;
import swaiotos.sal.system.ISystem;

import static android.content.Context.TELEPHONY_SERVICE;


public class AdjustReportDataUtils {
    private static AdjustReportData.Header header = null;

    public static synchronized  AdjustReportData getReportData(Context c,
                                                               String tag,
                                                               AdjustReportData.PayLoadData payLoadData,
                                                               AdjustReportData.TargetData targetData) {
        AdjustReportData data = new AdjustReportData();
        data.setHeader(updateHeader(c,tag,targetData));
        data.setPayload(payLoadData);
        return data;
    }

    private static AdjustReportData.Header updateHeader(Context c,String tag,AdjustReportData.TargetData targetData){
        if (header == null)
            header = getReportHeader(c);
        AdjustReportData.Header mHeader = new AdjustReportData.Header();
        mHeader.setClient(header.getClient());
        mHeader.setTimestamp(header.getTimestamp());
        mHeader.setTag(tag);

        if (targetData != null) {
            if (!TextUtils.isEmpty(targetData.getTargetType()))
                mHeader.getClient().setTargetType(targetData.getTargetType());
            if (!TextUtils.isEmpty(targetData.getTargetLsid()))
                mHeader.getClient().setTargetLsid(targetData.getTargetLsid());
        } else {
            mHeader.getClient().setTargetType(null);
            mHeader.getClient().setTargetLsid(null);
        }
        return mHeader;
    }

    private static AdjustReportData.Header getReportHeader(Context c) {
        if (null == header) {
            if (UserBehaviorAnalysis.deviceType.equals("tv") || UserBehaviorAnalysis.deviceType.equals("dongle")) {
                header = new AdjustReportData.Header();
                AdjustReportData.ClientDataByB clientData = new AdjustReportData.ClientDataByB();
                header.setClient(clientData);
                try {
                    clientData.setPkg(c.getPackageName());
                    if (Constants.isDangle()) {
                        clientData.setDeviceType("dongle");
                    } else {
                        clientData.setDeviceType("tv");
                    }
                    clientData.setDid(getActiveID(c));
                    clientData.setMac(getMac(c));
                    clientData.setSourceLsid(UserBehaviorAnalysis.lsid);
                    clientData.setChip(getcChip(c));
                    clientData.setModel(getcMode(c));
                    clientData.setBrand(getBrand(c));
                    clientData.setSysVersion(getsysVersion(c));
                    clientData.setAppVersion(getAppVersionCode(c) + "");
                    clientData.setWifiSSID(UserBehaviorAnalysis.wifiSSID);
//                    clientData.setTargetDid("disconnected");
//                    clientData.setTargetTel("");
//                    clientData.setTargetUserId("");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                header = new AdjustReportData.Header();
                AdjustReportData.ClientDataByM clientData = new AdjustReportData.ClientDataByM();
                header.setClient(clientData);

                clientData.setPkg(c.getPackageName());
                clientData.setAppVersion(""+getAppVersionCode(c));
                clientData.setSysVersion(""+Build.VERSION.SDK_INT);
                clientData.setBrand(""+Build.BRAND);
                clientData.setModel(""+Build.MODEL);
                clientData.setDeviceType("mobile-android");
                clientData.setDid(getMachineImei(c));
                clientData.setTel(UserBehaviorAnalysis.moblieNumber);
                clientData.setSourceLsid(UserBehaviorAnalysis.lsid);
                clientData.setUserId(UserBehaviorAnalysis.coocaaId);
                clientData.setWifiSSID(UserBehaviorAnalysis.wifiSSID);
                clientData.setTargetDid("disconnected");

            }
        }
        return header;
    }

    private static String MAC = "";

    public static String getMac(Context context) {
        if (!TextUtils.isEmpty(MAC))
            return MAC;
        try {
            try {
                IDeviceInfo deviceInfo = SAL.getModule(context, SalModule.DEVICE_INFO);
                MAC = deviceInfo.getMac();
            } catch (Exception e) {
                MAC = MACUtils.getMac(context);
            }
        } catch (Exception e) {
        }
        return MAC;
    }

    private static String activeID = "";

    public static String getActiveID(Context context) {
        if (!TextUtils.isEmpty(activeID))
            return activeID;
        try {
            ISystem iSystem = SAL.getModule(context, SalModule.SYSTEM);
            activeID = iSystem.getActiveId();
        } catch (Exception e) {
        }
        AndroidLog.androidLog("---getActiveID----"+activeID);
        return activeID;
    }

    private static String cChip = "";

    public static String getcChip(Context context) {
        if (!TextUtils.isEmpty(cChip))
            return cChip;
        try {
            IDeviceInfo deviceInfo = SAL.getModule(context, SalModule.DEVICE_INFO);
            cChip = deviceInfo.getChip();
        } catch (Exception e) {
        }
        return cChip;
    }

    private static String cMode = "";

    public static String getcMode(Context context) {
        if (!TextUtils.isEmpty(cMode))
            return cMode;
        try {
            IDeviceInfo deviceInfo = SAL.getModule(context, SalModule.DEVICE_INFO);
            cMode = deviceInfo.getModel();
        } catch (Exception e) {
        }
        return cMode;
    }

    public static int getAppVersionCode(Context context) {
        int versionCode = -1;
        PackageManager pm = context.getPackageManager();
        PackageInfo info;
        try {
            if (pm != null) {
                info = pm.getPackageInfo(context.getPackageName(), 0);
                versionCode = info.versionCode;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    private static String sysversion = "";
    private static String brand = "";

    public static String getsysVersion(Context context) {

        if (!TextUtils.isEmpty(sysversion))
            return sysversion;
        try {
            ISystemInfo iSystemInfo = SAL.getModule(context, SalModule.SYSTEM_INFO);
            long versionCode = iSystemInfo.getVersionCode();
            sysversion = String.valueOf(versionCode);
        } catch (Exception e) {
        }
        return sysversion;
    }

    public static String getBrand(Context context) {

        if (!TextUtils.isEmpty(brand))
            return brand;
        try {
            IDeviceInfo deviceInfo = SAL.getModule(context, SalModule.DEVICE_INFO);
            brand = deviceInfo.getBrand();
        } catch (Exception e) {
        }
        return brand;
    }


    public static String getSkyform(){
        try {
            String skyform = SkySystemProperties.getProperty("ro.build.skyform");
//            Log.d("state","getSkyform skyform:"+skyform);
            return skyform;
        }catch (Exception e){
            return "";
        }
    }

    private static String imei = "";

    public static String getMachineImei(Context context) {
        if (!TextUtils.isEmpty(imei)) {
            return imei;
        }
        TelephonyManager manager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
        Class clazz = manager.getClass();
        String imei = "";
        try {
            Method getImei=clazz.getDeclaredMethod("getImei",int.class);//(int slotId)
            getImei.setAccessible(true);
            imei = (String) getImei.invoke(manager);
        } catch (Exception e) {
        }
        return imei;
    }
}
