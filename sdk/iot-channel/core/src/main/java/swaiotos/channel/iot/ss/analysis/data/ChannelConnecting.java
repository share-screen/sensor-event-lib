package swaiotos.channel.iot.ss.analysis.data;

/**
 * @ProjectName: iot-channel-app
 * @Package: swaiotos.channel.iot.ss.analysis.data
 * @ClassName: ChannelConnecting
 * @Description: java类作用描述
 * @Author: wangyuehui
 * @CreateDate: 2021/6/18 10:39
 * @UpdateUser: 更新者
 * @UpdateDate: 2021/6/18 10:39
 * @UpdateRemark: 更新说明
 * @Version: 1.0
 */
public class ChannelConnecting {
    public static final String EVENT_NAME = "linkConnect";
    private String channelType;//local/sse

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }
}
