package swaiotos.channel.iot.ss.analysis;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import swaiotos.channel.iot.ss.SSChannelService;
import swaiotos.channel.iot.ss.analysis.data.ChannelConnecting;
import swaiotos.channel.iot.ss.analysis.data.ChannelDispathMsg;
import swaiotos.channel.iot.ss.analysis.data.LocalConnectError;
import swaiotos.channel.iot.ss.analysis.data.LocalConnectSuccess;
import swaiotos.channel.iot.ss.analysis.data.SSConnect;
import swaiotos.channel.iot.ss.analysis.data.SSeInit;
import swaiotos.channel.iot.ss.analysis.data.SSeInitError;
import swaiotos.channel.iot.ss.analysis.data.SSeMsgError;
import swaiotos.channel.iot.ss.analysis.data.ServerInterfaceMsg;
import swaiotos.channel.iot.ss.analysis.data.ServerInterfaceMsgError;
import swaiotos.channel.iot.ss.server.data.log.AdjustReportData;
import swaiotos.channel.iot.ss.server.data.log.AdjustReportDataUtils;
import swaiotos.channel.iot.ss.server.http.SessionHttpService;
import swaiotos.channel.iot.ss.server.http.api.HttpApi;
import swaiotos.channel.iot.ss.server.http.api.HttpResult;
import swaiotos.channel.iot.ss.server.http.api.HttpSubscribe;
import swaiotos.channel.iot.ss.server.http.api.HttpThrowable;
import swaiotos.channel.iot.utils.AndroidLog;

public class UserBehaviorAnalysis {
	public static final String EVENT_NAME_NET_STATE_CHANGE = "netStateChange";
	public static final String EVENT_NAME_LINK_MODULE_INIT = "linkModuleInit";
	private static ExecutorService singleThreadExecutor;
	private static LinkedBlockingQueue<AdjustReportData> blockingQueue;
	private static boolean isRunning;
	private static Future<?> future;
	public static String deviceType = "";//上报终端类型，mobile(手机）、dongle、tv、panel；
	public static String lsid = "";
	public static String wifiSSID = "";
	public static String moblieNumber = "";
	public static String coocaaId = "";
	public static String userId = "";

	public static void init() {

		blockingQueue = new LinkedBlockingQueue<>();
		singleThreadExecutor = Executors.newSingleThreadExecutor();
		future = singleThreadExecutor.submit(new Runnable() {

			@Override
			public void run() {

				Thread.currentThread().setName("behavior-thread");

				isRunning = true;
				final AtomicBoolean hasGet = new AtomicBoolean(true);
				final HttpSubscribe<HttpResult<Void>> callback = new HttpSubscribe<HttpResult<Void>>() {
					@Override
					public void onSuccess(HttpResult<Void> result) {
						synchronized (hasGet) {
							hasGet.set(true);
							hasGet.notifyAll();
						}
					}

					@Override
					public void onError(HttpThrowable error) {
						synchronized (hasGet) {
							hasGet.set(true);
							hasGet.notifyAll();
						}
					}
				};

				try {
					while (isRunning) {

						// get a request
						AdjustReportData reportData = blockingQueue.take();

						if (null == reportData) {
							continue;
						}

						AndroidLog.androidLog("blockingQueue, take a request, command---");

						while (!hasGet.get()) {
							synchronized (hasGet) {
								try {
									hasGet.wait(500);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
						}
						synchronized (hasGet) {
							hasGet.set(false);
						}
						HttpApi.getInstance().request(SessionHttpService.SERVICE.reportAdJustLog(reportData),callback,"","");
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
				}
			}
		});
	}

	public static void unInit() {

		isRunning = false;
		if (future != null) {
			future.cancel(true);
		}
		if (singleThreadExecutor != null) {
			singleThreadExecutor.shutdown();
		}

	}

	public static <T> void appEventBehavior (String eventName, T data, AdjustReportData.TargetData targetData) {

		AdjustReportData.PayLoadData<T> pData = new AdjustReportData.PayLoadData<T>();
		AdjustReportData.EventData<T> eData = new AdjustReportData.EventData<T>();
		if (data != null) {
			eData.setData(data);
		}
		eData.setEventName(eventName);
		eData.setEventTime(System.currentTimeMillis());
		pData.setEvents(new ArrayList<>());
		pData.getEvents().add(eData);
		if (SSChannelService.getContext() != null) {
			AdjustReportData reportData = AdjustReportDataUtils.getReportData(SSChannelService.getContext(), "iotchannel.link_events", pData,targetData);
			blockingQueue.offer(reportData);
		}
	}

	public static void reportServerInterfaceSuccess(String sourceLSID, long time,String interfaceName) {
		ServerInterfaceMsg serverInterfaceMsg = new ServerInterfaceMsg();
		serverInterfaceMsg.setSourceLsid(sourceLSID);
		serverInterfaceMsg.setTime(time);
		serverInterfaceMsg.setMethod(interfaceName);
		serverInterfaceMsg.setDeviceType(deviceType);
		serverInterfaceMsg.setWifiSSID(wifiSSID);

		appEventBehavior(ServerInterfaceMsg.EVENT_NAME,serverInterfaceMsg,null);
	}

	public static void reportServerInterfaceError(String sourceLSID,String method,String errorCode,String errorDsc) {

		ServerInterfaceMsgError serverInterfaceMsgError = new ServerInterfaceMsgError();
		serverInterfaceMsgError.setSourceLsid(sourceLSID);
		serverInterfaceMsgError.setMethod(method);
		serverInterfaceMsgError.setErrorCode(errorCode);
		serverInterfaceMsgError.setErrorDsc(errorDsc);
		serverInterfaceMsgError.setDeviceType(deviceType);
		serverInterfaceMsgError.setWifiSSID(wifiSSID);
		try {
			AndroidLog.androidLog("reportServerInterfaceError--:"+ JSON.toJSONString(serverInterfaceMsgError));
		} catch (Exception e) {

		}
//		appEventBehavior(ServerInterfaceMsgError.EVENT_NAME,serverInterfaceMsgError);  //去除接口请求
	}


	public static void reportSSConnect(String sourceLSID,String targetLSID,long time) {
		SSConnect ssConnect = new SSConnect();
		ssConnect.setSourceLsid(sourceLSID);
		ssConnect.setTargetLsid(targetLSID);
		ssConnect.setTime(time);
		ssConnect.setDeviceType(deviceType);
		ssConnect.setWifiSSID(wifiSSID);

		appEventBehavior(SSConnect.EVENT_NAME,ssConnect,null);
	}

	public static void reportLocalConnect(String sourceLSID,long time) {
		LocalConnectSuccess localConnectSuccessData = new LocalConnectSuccess();
		localConnectSuccessData.setSourceLsid(sourceLSID);
		localConnectSuccessData.setTime(time);
		localConnectSuccessData.setDeviceType(deviceType);
		localConnectSuccessData.setWifiSSID(wifiSSID);

		appEventBehavior(LocalConnectSuccess.EVENT_NAME,localConnectSuccessData,null);
	}

	public static void reportLocalConnectError(String sourceLSID,String targetLSID) {
		LocalConnectError localConnectError = new LocalConnectError();
		localConnectError.setSourceLsid(sourceLSID);
		localConnectError.setTargetLsid(targetLSID);
		localConnectError.setErrorCode("-1");
		localConnectError.setErrorDsc("send failure");
		localConnectError.setDeviceType(deviceType);
		localConnectError.setWifiSSID(wifiSSID);

		appEventBehavior(LocalConnectError.EVENT_NAME,localConnectError,null);
	}

	public static void reportSSeMsgError(String sourceLSID,String targetLSID, String msgId,String type,String dsc,String cmdType,String content) {
		SSeMsgError sSeMsgError = new SSeMsgError();
		sSeMsgError.setSourceLsid(sourceLSID);
		sSeMsgError.setTargetLsid(targetLSID);
		sSeMsgError.setMsgID(msgId);
		sSeMsgError.setErrorCode("-1");
		sSeMsgError.setMsgType(type);
		sSeMsgError.setErrorDsc(dsc);
		sSeMsgError.setCmdType(cmdType);
		sSeMsgError.setDeviceType(deviceType);
		sSeMsgError.setWifiSSID(wifiSSID);
		sSeMsgError.setContent(content);

		appEventBehavior(SSeMsgError.EVENT_NAME,sSeMsgError,null);
	}

	public static void reportSSeInitTime(String sourceLSID, long time) {
		SSeInit sSeInit = new SSeInit();
		sSeInit.setSourceLsid(sourceLSID);
		sSeInit.setDeviceType(deviceType);
		sSeInit.setWifiSSID(wifiSSID);
		sSeInit.setTime(time);

		appEventBehavior(SSeInit.EVENT_NAME,sSeInit,null);
	}

	public static void reportSSeInitError(String sourceLSID, String errorCode,String errorDsc) {
		SSeInitError sSeInitError = new SSeInitError();
		sSeInitError.setSourceLsid(sourceLSID);
		sSeInitError.setDeviceType(deviceType);
		sSeInitError.setWifiSSID(wifiSSID);
		sSeInitError.setErrorCode(errorCode);
		sSeInitError.setErrorDsc(errorDsc);

		appEventBehavior(SSeInitError.EVENT_NAME,sSeInitError,null);
	}

	/**
	 * 大屏段 通道初始化
	 *
	 * */
	public static void reportChannelInit() {
		appEventBehavior(EVENT_NAME_LINK_MODULE_INIT,null,null);
	}

	/**
	 *
	 * 大屏端  被连接
	 *
	 * */
	public static void reportChannelConnecting(String channelType,String targetType,String targetLsid) {
		ChannelConnecting channelConnecting = new ChannelConnecting();
		channelConnecting.setChannelType(channelType);
		AdjustReportData.TargetData targetData = new AdjustReportData.TargetData();
		targetData.setTargetType(targetType);
		targetData.setTargetLsid(targetLsid);
		appEventBehavior(ChannelConnecting.EVENT_NAME,channelConnecting,targetData);
	}

	/**
	 *
	 * 大屏端  被连接
	 *
	 * */
	public static void reportChannelDispathMsg(String serverId,String targetType,String targetLsid) {
		ChannelDispathMsg channelDispathMsg = new ChannelDispathMsg();
		channelDispathMsg.setSeviceId(serverId);
		AdjustReportData.TargetData targetData = new AdjustReportData.TargetData();
		targetData.setTargetType(targetType);
		targetData.setTargetLsid(targetLsid);
		appEventBehavior(ChannelDispathMsg.EVENT_NAME,channelDispathMsg,targetData);
	}

	/**
	 * 大屏段 通道初始化
	 *
	 * */
	public static void reportChannelNetStateChange() {
		appEventBehavior(EVENT_NAME_NET_STATE_CHANGE,null,null);
	}

}
