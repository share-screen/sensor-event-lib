package swaiotos.channel.iot.ss.analysis.data;

/**
 * @ProjectName: iot-channel-app
 * @Package: swaiotos.channel.iot.ss.analysis.data
 * @ClassName: ChannelDispathMsg
 * @Description: java类作用描述
 * @Author: wangyuehui
 * @CreateDate: 2021/6/18 11:20
 * @UpdateUser: 更新者
 * @UpdateDate: 2021/6/18 11:20
 * @UpdateRemark: 更新说明
 * @Version: 1.0
 */
public class ChannelDispathMsg {
    public static final String EVENT_NAME = "linkDispathMsg";

    private String seviceId;

    public String getSeviceId() {
        return seviceId;
    }

    public void setSeviceId(String seviceId) {
        this.seviceId = seviceId;
    }
}
