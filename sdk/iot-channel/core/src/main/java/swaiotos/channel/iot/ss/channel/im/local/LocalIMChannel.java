package swaiotos.channel.iot.ss.channel.im.local;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import swaiotos.channel.iot.ss.SSChannel;
import swaiotos.channel.iot.ss.SSContext;
import swaiotos.channel.iot.ss.analysis.UserBehaviorAnalysis;
import swaiotos.channel.iot.ss.channel.base.BaseChannel;
import swaiotos.channel.iot.ss.channel.base.local.LocalChannel;
import swaiotos.channel.iot.ss.channel.im.IMChannel;
import swaiotos.channel.iot.ss.channel.im.IMMessage;
import swaiotos.channel.iot.ss.channel.im.IMMessageCallback;
import swaiotos.channel.iot.ss.channel.stream.IStreamChannel;
import swaiotos.channel.iot.ss.config.PortConfig;
import swaiotos.channel.iot.ss.controller.ControllerServerImpl;
import swaiotos.channel.iot.ss.device.PhoneDeviceInfo;
import swaiotos.channel.iot.ss.device.TVDeviceInfo;
import swaiotos.channel.iot.ss.server.utils.Constants;
import swaiotos.channel.iot.ss.session.Session;
import swaiotos.channel.iot.utils.AndroidLog;
import swaiotos.channel.iot.utils.IpV4Util;
import swaiotos.channel.iot.utils.SyncObject;
import swaiotos.channel.iot.utils.ThreadManager;

/**
 * @ClassName: LocalIMChannel
 * @Author: lu
 * @CreateDate: 2020/4/10 3:54 PM
 * @Description:
 */
public class LocalIMChannel implements IMChannel, IStreamChannel.Receiver, BaseChannel.Callback {
    private static final String TAG = "local-im";

    class Client implements IStreamChannel.SenderMonitor {
        public final String lsid;
        public final String ip;
        public final int port;
        public IStreamChannel.Sender sender;

        public Client(LocalChannel channel, String lsid, String ip,
                      int port, TcpClientResult callback, Receiver receiver) {
            this.lsid = lsid;
            this.ip = ip;
            this.port = port;
            this.sender = newSender(lsid,channel, ip, port, callback, receiver);
        }

        private IStreamChannel.Sender newSender(String lsid, LocalChannel channel, String ip, int port,
                                                TcpClientResult callback, Receiver receiver) {
            IStreamChannel.Sender sender = channel.openSender(lsid,ip, port, callback, receiver);
            sender.setSenderMonitor(this);
            AndroidLog.androidLog("newSender create");
            return sender;
        }

        boolean available() {
            return sender.available();
        }

        @Override
        public String toString() {
            return "Client[" + lsid + "@" + ip + ":" + port + "]";
        }

        @Override
        public void onAvailableChanged(boolean available) {
            Log.d(TAG, this + " onAvailableChanged " + available);
        }
    }

    private final SSContext mSSContext;
    private final LocalChannel mLocalChannel;
    private String mAddress;
    private int port;
    private Receiver mReceiver;
    private final Map<String, SyncObject<ControllerServerImpl.Message>> mSyncMessages = new LinkedHashMap<>();

    private final Map<String, Client> mClients = new ConcurrentHashMap<>();

    public LocalIMChannel(SSContext context, LocalChannel localChannel) {
        mSSContext = context;
        mLocalChannel = localChannel;
        mLocalChannel.addCallback(this);
    }

    @Override
    public String open() throws IOException {
        mLocalChannel.addCallback(this);
        performOpen(false);
        return getAddress();
    }

    private void performOpen(boolean notify) {
        port = mLocalChannel.openReceiver(this);
        update(notify);
    }

    private void update(boolean notify) {
        String ip = mLocalChannel.getAddress();
        if (TextUtils.isEmpty(ip)) {
            mAddress = null;
        } else {
            mAddress = ip + ":" + port;
        }
    }

    @Override
    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    @Override
    public String getAddress() {
        return mAddress;
    }

    @Override
    public boolean available() {
        return mLocalChannel.available();
    }

    @Override
    public void close() throws IOException {
        mLocalChannel.removeCallback(this);
        performClose();
    }

    private void performClose() {
        mLocalChannel.closeReceiver(port);
        synchronized (mClients) {
            Collection<Client> clients = mClients.values();
            for (Client client : clients) {
                mLocalChannel.closeSender(client.sender);
            }
            mClients.clear();
        }
        update(true);
    }

    @Override
    public void openClient(final Session session, final TcpClientResult callback) {
        final String lsid = session.getId();
        synchronized (mClients) {
            Client client = mClients.get(lsid);

            if (client != null) {
                closeLocalClient(lsid, client);
                Log.d("yao","reOpenLocalClient remove");
            }

            Log.d("yao","openClient lsid:" + lsid);
            final String ip = session.getExtra(SSChannel.STREAM_LOCAL);
            if (TextUtils.isEmpty(ip)) {
                mSSContext.postDelay(new Runnable() {
                    @Override
                    public void run() {
                        mSSContext.getSessionManager().connectingChannelSessionState(Constants.COOCAA_IOT_CHANNEL_TYPE_LOCAL,
                                Constants.COOCAA_IOT_CHANNEL_TYPE_CONNECTED);
                        mSSContext.getSessionManager().connectChannelSessionState(
                                Constants.COOCAA_IOT_CHANNEL_TYPE_LOCAL, Constants.COOCAA_IOT_CHANNEL_STATE_DISCONNECT);
                        if (callback != null) {
                            callback.onResult(-2, "local tcp client fail");
                        }
                    }
                }, 10);

                return;
            }

            mSSContext.getSessionManager().connectingChannelSessionState(Constants.COOCAA_IOT_CHANNEL_TYPE_LOCAL,
                    Constants.COOCAA_IOT_CHANNEL_TYPE_CONNECTING);

            ThreadManager.getInstance().ioThread(new Runnable() {
                @Override
                public void run() {
                    int port = PortConfig.getLocalServerPort(mSSContext.getContext().getPackageName());
                    Client client = new Client(mLocalChannel, lsid, ip, port, new TcpClientResult() {
                        @Override
                        public void onResult(int code, String message) {
                            if (code == 0 && mSSContext.getDeviceInfo() instanceof PhoneDeviceInfo) {
                                sendConnectMessage(session,lsid,2000,callback);
                            } else {
                                if (callback != null)
                                    callback.onResult(code,message);
                            }

                        }
                    }, mReceiver);
                    mClients.put(lsid, client);
                }
            });
        }
    }

    @Override
    public void reOpenLocalClient(final Session session) {
        AndroidLog.androidLog("reOpenLocalClient start");

        final String lsid = session.getId();
        synchronized (mClients) {
            final Client client = mClients.get(lsid);
            if (client != null) {
                closeLocalClient(lsid, client);
                AndroidLog.androidLog("reOpenLocalClient remove");
            }

            final String ip = session.getExtra(SSChannel.STREAM_LOCAL);
            if (TextUtils.isEmpty(ip) || ip.equals("null")) {
                AndroidLog.androidLog("reOpenLocalClient error and ip is empty");
                mSSContext.getSessionManager().connectingChannelSessionState(Constants.COOCAA_IOT_CHANNEL_TYPE_LOCAL,
                        Constants.COOCAA_IOT_CHANNEL_TYPE_CONNECTED);
                mSSContext.getSessionManager().connectChannelSessionState(
                        Constants.COOCAA_IOT_CHANNEL_TYPE_LOCAL, Constants.COOCAA_IOT_CHANNEL_STATE_DISCONNECT);
                return;
            }

//            mSSContext.getSessionManager().connectingChannelSessionState(Constants.COOCAA_IOT_CHANNEL_TYPE_LOCAL,
//                    Constants.COOCAA_IOT_CHANNEL_TYPE_CONNECTING);
            int port = PortConfig.getLocalServerPort(mSSContext.getContext().getPackageName());
            Client newClient = new Client(mLocalChannel, lsid, ip, port, new TcpClientResult() {
                @Override
                public void onResult(int code, String message) {
                    if (code == 0 && mSSContext.getDeviceInfo() instanceof PhoneDeviceInfo) {
                        sendConnectMessage(session,lsid,2000,null);
                    }
                }
            }, mReceiver);
            mClients.put(lsid, newClient);
            AndroidLog.androidLog("reOpenLocalClient end");
        }
    }

    @Override
    public void reOpenSSE() {
        //do nothing
    }

    @Override
    public boolean available(Session session) {
        String lsid = session.getId();
        synchronized (mClients) {
            Client client = mClients.get(lsid);
            if (client != null) {
                return client.available();//mLocalChannel.available(client.sender);
            }
        }
//        try {
//            //兼容方案：共享屏双通道优化-热点连接或同wifi连接后，可音量加减，但投屏失败，手机端提示超时  bug fixed!
//            Session connectSession = mSSContext.getSessionManager().getConnectedSession();
//            if (!TextUtils.isEmpty(lsid)
//                    && connectSession != null
//                    && connectSession.getId() != null
//                    && lsid.equals(connectSession.getId())) {
//                mSSContext.getSessionManager().connectChannelSessionState(
//                        Constants.COOCAA_IOT_CHANNEL_TYPE_LOCAL, Constants.COOCAA_IOT_CHANNEL_STATE_DISCONNECT);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        Log.d("yao", "LocalIMChannel available session client is null : "+lsid);
        return false;
    }

    @Override
    public void closeClient(Session session, boolean forceClose) {
        String lsid = session.getId();
        synchronized (mClients) {
            Client client = mClients.get(lsid);
            if (client != null) {
                if (forceClose) {
                    closeLocalClient(lsid, client);
                    client = null;
                    System.gc();
                } else {
                    if (!client.available()) {
                        closeLocalClient(lsid, client);
                        client = null;
                        System.gc();
                    }
                }

            }

        }
    }

    private void closeLocalClient(String lsid, Client client) {
        AndroidLog.androidLog("yao---lsid:"+lsid + "----closeLocalClient:"+client);
        if (TextUtils.isEmpty(lsid) || client == null) {
            return;
        }
        mLocalChannel.closeSender(client.sender);
        mClients.remove(lsid);
    }

    @Override
    public void send(final IMMessage message, IMMessageCallback callback) throws Exception {
        Session target = message.getTarget();
        send(target, message, callback);
    }

    @Override
    public void send(IMMessage message) throws Exception {
        try {
            send(message, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send(Session target, IMMessage message) throws Exception {
        send(target, message, null);
    }

    @Override
    public void send(Session target, IMMessage message, IMMessageCallback callback) throws Exception {
        Client client = getClient(target);
        if (client != null) {
            if (callback != null) {
                callback.onStart(message);
            }
            client.sender.send(message, callback);
        } else {
            if (callback != null) {
                callback.onEnd(message, -1, "tcp local send error");
            }
        }
    }

    @Override
    public boolean serverSend(IMMessage message, IMMessageCallback callback) throws Exception {
        if (mLocalChannel != null) {
            return mLocalChannel.sendServerMessage(message);
        }
        return false;
    }

    @Override
    public List<String> serverSendList() throws Exception {
        if (mLocalChannel != null) {
            return mLocalChannel.serverConnectList();
        }
        return null;
    }

    @Override
    public void removeServerConnect(String sid) {
        if (mLocalChannel != null) {
            mLocalChannel.removeServerConnect(sid);
        }
    }

    @Override
    public void onReceive(byte[] data) {
        String content = new String(data);
        if (!TextUtils.isEmpty(content) && (content.contains(ControllerServerImpl.CMD.REPLY.toString()) ||
                content.contains(ControllerServerImpl.CMD.CONNECT.toString()))) {
            try {
                ControllerServerImpl.Message msg = new ControllerServerImpl.Message(content);
                switch (msg.getCmd()) {
                    case CONNECT: {
                        handleConnect(msg);
                        break;
                    }
                    case REPLY: {
                        handleReply(msg);
                        break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return;
        }
        try {
            IMMessage message = IMMessage.Builder.decode(content);
            if (mReceiver != null) {
                mReceiver.onReceive(this, message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void closeReceiveClient(String sid) {
        Client client = mClients.get(sid);
        Log.d("yao","---closeReceiveClient:"+sid + " client:"+client);
        if (client != null) {
            closeLocalClient(sid, client);
        }
        //重试1次
        try {
            Session connectedSession = mSSContext.getSessionManager().getConnectedSession();
            if (connectedSession != null
                    && !TextUtils.isEmpty(connectedSession.getId())
                    && connectedSession.getId().equals(sid)) {
                Log.d("yao","closeReceiveClient---retry--:"+sid);
                reOpenLocalClient(connectedSession);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public String type() {
        return SSChannel.STREAM_LOCAL;
    }

    private Client getClient(Session session) {
        String lsid = session.getId();
        synchronized (mClients) {
            return mClients.get(lsid);
        }
    }

    private void resetConnections() {
        try {
            Session session = mSSContext.getSessionManager().getConnectedSession();
            if (session != null) {
                reOpenLocalClient(session);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(BaseChannel channel) {
        ThreadManager.getInstance().ioThread(new Runnable() {
            @Override
            public void run() {
                //performOpen(true);  //add by colin ,切换网络，socket server不变
                update(true);
                resetConnections();
            }
        });
    }

    @Override
    public void onDisconnected(BaseChannel channel) {
        ThreadManager.getInstance().ioThread(new Runnable() {
            @Override
            public void run() {
                //performClose(); //add by colin ,切换网络，socket server不变
                Collection<Client> clients = mClients.values();
                for (Client client : clients) {
                    mLocalChannel.closeSender(client.sender);
                }
                mClients.clear();

                mSSContext.getSessionManager().connectingChannelSessionState(Constants.COOCAA_IOT_CHANNEL_TYPE_LOCAL,
                        Constants.COOCAA_IOT_CHANNEL_TYPE_CONNECTED);
                mSSContext.getSessionManager().connectChannelSessionState(
                        Constants.COOCAA_IOT_CHANNEL_TYPE_LOCAL, Constants.COOCAA_IOT_CHANNEL_STATE_DISCONNECT);
                //update(true);
            }
        });
    }

    private boolean checkCommonNet(Session session) {
        try {
            Session mySession = mSSContext.getSessionManager().getMySession();
            if (session == null || session.getExtras().size() <= 0
                    || mySession == null || mySession.getExtras().size() <= 0)
                return false;
            String sessionIP = session.getExtras().get(SSChannel.STREAM_LOCAL);
            Map<String, String> extras = mySession.getExtras();
            String mySessionIP = extras.get(SSChannel.STREAM_LOCAL);


            Log.e("yao", "LocalIMChannel checkCommonNet=mySessionIP:" + mySessionIP + "  target ip=" + sessionIP);
            if (TextUtils.isEmpty(sessionIP) || TextUtils.isEmpty(mySessionIP)) {
                return false;
            }

            if (IpV4Util.checkSameSegmentByDefault(sessionIP, mySessionIP)) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void sendConnectMessage(Session session,String lsid,long timeout, TcpClientResult callback) {
        try {
            Client client = mClients.get(lsid);
            if (client != null) {
                if (!Constants.IOT_IS_DOUBLE_CHANNEL) {
                    Log.d("yao","go to single channel");
                    client.sender.localConnected(true);
                    if (callback != null) {
                        callback.onResult(0,"success");
                    }
                    return;
                }
                Log.d("yao","go to double channel");
                String source = mSSContext.getLSID();
                String mySessionStr = mSSContext.getSessionManager().getMySession().encode();
                ControllerServerImpl.Message message = new ControllerServerImpl.Message(ControllerServerImpl.CMD.CONNECT, source);
                message.putPayload("session",mySessionStr);

                ControllerServerImpl.Message replyMessage = sendSync(message.getId(),message.toString(),timeout,client);
                if (replyMessage != null) {
                    client.sender.localConnected(true);
                    if (callback != null) {
                        callback.onResult(0,"success");
                    }
                } else {
                    String protoVersion = session.getExtra("protoVersion");
                    if (!TextUtils.isEmpty(protoVersion)) {
                        client.sender.localConnected(false);
                        closeLocalClient(lsid,client);
                        if (callback != null) {
                            callback.onResult(-3,"double channel connect failure!");
                        }
                    } else {
                        client.sender.localConnected(true);
                        if (callback != null) {
                            callback.onResult(0,"success");
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private ControllerServerImpl.Message sendSync(String msgId, String message, long timeout, Client client) {
        SyncObject<ControllerServerImpl.Message> object;
        synchronized (mSyncMessages) {
            object = mSyncMessages.get(msgId);
            if (object == null) {
                object = new SyncObject<>();
                mSyncMessages.put(msgId, object);
                try {
                    client.sender.send(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return object.get(timeout);
    }

    private void handleReply(ControllerServerImpl.Message message) {
        Log.d("yao", "handleReply = " + message.getSource());

        SyncObject<ControllerServerImpl.Message> object;
        synchronized (mSyncMessages) {
            object = mSyncMessages.get(message.getId());
            mSyncMessages.remove(message.getId());
        }
        if (object != null) {
            object.set(message);
        }
    }

    private void handleConnect(ControllerServerImpl.Message message) {
        Log.d("yao", "handleConnect = " + message.getSource());
        try {
            Session source = Session.Builder.decode(message.getPayload("session"));
            String sid = mSSContext.getLSID();

            //日志上报
            if (mSSContext.getDeviceInfo() != null && mSSContext.getDeviceInfo() instanceof TVDeviceInfo) {
                UserBehaviorAnalysis.reportChannelConnecting(
                        "local",
                        "mobile",
                        source.getId());

                //友盟上报
//                Map<String,Object> commonProperty = Constants.uMenCommonProperty(
//                        mSSContext.getContext(),
//                        "mobile");
//                Map<String,Object> property = new HashMap<>(commonProperty);
//                property.put("channelType","local");
//                MobclickAgent.onEventObject(mSSContext.getContext(),
//                        ChannelConnecting.EVENT_NAME,
//                        property);
            }

            Log.d("sse", "handleConnect replay msg source:" + sid);
            final ControllerServerImpl.Message replyMessage  = message.reply(sid);

            replyMessage.putPayload("session", mSSContext.getSessionManager().getMySession().encode());
            TcpClientResult tcpClientResult = new TcpClientResult() {
                @Override
                public void onResult(int code, String message) {
                    if (code == 0) {
                        Client client = getClient(source);
                        if (client != null) {
                            try {
                                client.sender.localConnected(true);
                                client.sender.send(replyMessage.toString());

                                mSSContext.getSessionManager().addServerSession(source);
                                mSSContext.getSessionManager().updateSession(source);

                                //记录source到配置文件
                                mSSContext.getSessionManager().saveHandlerConnectSession(source.encode());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            };
            final String lsid = source.getId();
            final Client client = mClients.get(lsid);
            if (client != null) {
                closeLocalClient(lsid, client);
                AndroidLog.androidLog("reOpenLocalClient remove");
            }

            openClient(source, tcpClientResult);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("sse", "replay handleConnect---" + e.getMessage());
        }

    }

}
