package swaiotos.channel.iot.ss.server.data.log;

import java.io.Serializable;
import java.util.List;

/**
 */
public class AdjustReportData implements Serializable {

    private Header header;
    private PayLoadData payload;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public PayLoadData getPayload() {
        return payload;
    }

    public void setPayload(PayLoadData payload) {
        this.payload = payload;
    }

    public static class Header implements Serializable {
        private String tag;
        private long timestamp;
        private ClientData client;

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        public ClientData getClient() {
            return client;
        }

        public void setClient(ClientData client) {
            this.client = client;
        }
    }

    public static class ClientDataByB extends ClientData {
        private String chip;        //机芯
        private String mac;         //mac地址
        private String targetTel;   //目标设备的电话号码
        private String targetUserId;//目标设备的用户账号ID，酷开ID；

        public String getChip() {
            return chip;
        }

        public void setChip(String chip) {
            this.chip = chip;
        }

        public String getMac() {
            return mac;
        }

        public void setMac(String mac) {
            this.mac = mac;
        }

        public String getTargetTel() {
            return targetTel;
        }

        public void setTargetTel(String targetTel) {
            this.targetTel = targetTel;
        }

        public String getTargetUserId() {
            return targetUserId;
        }

        public void setTargetUserId(String targetUserId) {
            this.targetUserId = targetUserId;
        }
    }

    public static class ClientDataByM extends ClientData{
        private String tel;         //用户手机号码
        private String userId;      //用户账号的酷开ID；

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }

    public static class ClientData implements Serializable {

        private String pkg;         //应用包名
        private String appVersion;  //软件版本
        private String sysVersion;  //系统版本
        private String model;       //机型
        private String deviceType;  //上报终端类型，mobile-android、mobile-ios
        private String did;         //手机的唯一设备ID；
        private String sourceLsid;  //sid
        private String brand;
        private String wifiSSID;

        private String targetType;  //目标设备类型，TV/Dongle;
        private String targetDid;   //目标设备（Dongle/TV)的激活ID，无则提交disconnected；
        private String targetLsid;  //连接通道中大屏端的Lsid，若有的话；

        public String getPkg() {
            return pkg;
        }

        public void setPkg(String pkg) {
            this.pkg = pkg;
        }

        public String getAppVersion() {
            return appVersion;
        }

        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }

        public String getSysVersion() {
            return sysVersion;
        }

        public void setSysVersion(String sysVersion) {
            this.sysVersion = sysVersion;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDid() {
            return did;
        }

        public void setDid(String did) {
            this.did = did;
        }

        public String getSourceLsid() {
            return sourceLsid;
        }

        public void setSourceLsid(String sourceLsid) {
            this.sourceLsid = sourceLsid;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getWifiSSID() {
            return wifiSSID;
        }

        public void setWifiSSID(String wifiSSID) {
            this.wifiSSID = wifiSSID;
        }

        public String getTargetType() {
            return targetType;
        }

        public void setTargetType(String targetType) {
            this.targetType = targetType;
        }

        public String getTargetDid() {
            return targetDid;
        }

        public void setTargetDid(String targetDid) {
            this.targetDid = targetDid;
        }

        public String getTargetLsid() {
            return targetLsid;
        }

        public void setTargetLsid(String targetLsid) {
            this.targetLsid = targetLsid;
        }
    }


    public static class PayLoadData<T> implements Serializable{
        private List<EventData<T>> events;

        public List<EventData<T>> getEvents() {
            return events;
        }

        public void setEvents(List<EventData<T>> events) {
            this.events = events;
        }
    }

    public static class EventData<T> implements Serializable{
        private String eventName;
        private long eventTime;
        private T data;

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public long getEventTime() {
            return eventTime;
        }

        public void setEventTime(long eventTime) {
            this.eventTime = eventTime;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }
    }

    public static class TargetData implements Serializable {
        private String targetType;  //目标设备类型，mobile_android、mobile_ios、PC
        private String targetLsid;  //连接通道中目标设备的LSID；

        public String getTargetType() {
            return targetType;
        }

        public void setTargetType(String targetType) {
            this.targetType = targetType;
        }

        public String getTargetLsid() {
            return targetLsid;
        }

        public void setTargetLsid(String targetLsid) {
            this.targetLsid = targetLsid;
        }
    }

}
